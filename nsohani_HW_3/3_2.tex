\exercise{3.2} \emph{Basic feasible rays are extreme rays}\\
\noindent
\emph{Every basic feasible ray of the standard form problem is an extreme ray of its feasible region.}

\begin{proof}
\noindent
We proceed to prove this claim by contradiction. In order for a ray $\hat{z}$ to be an extreme ray of $S$, we cannot express that $\hat{z}$ as a linear combination of two other rays $z^1$ and $z^2$.

\bigskip
\noindent
Suppose that we can in fact express $\hat{z}$, which is assumed to be a basic feasible ray, as a linear combination of rays $z^1$ and $z^2$ where $z^1$ and $z^2$ are linearly independent, i.e.
\[
\tag{E3.2.1}\label{e321}
\hat{z} = z^1 + z^2, \hspace{10pt} z^1 \neq \mu z^2
\]
Because both $z^1$ and $z^2$ are assumed to be rays of the feasible region, we can express each as a linear combination of a spanning set, $z^i$, of the null space of $A$:
\[
\tag{E3.2.2}\label{e322}
\begin{array}{rrl}
z^1&=&\mathlarger{\sum_{i=1}^{n-m} \alpha_i z^i}\\
z^2&=&\mathlarger{\sum_{i=1}^{n-m} \gamma_i z^i}\\
\end{array}
\]
Without loss of generality, we can let the spanning set be the set of basic feasible rays. This is a convenient choice because of the compatible structure with $\hat{z}$. Therefore, we say the following:
\[
\tag{E3.2.3}\label{e323}
\mathlarger{z^i = \begin{bmatrix}
	e_i\\
	-A_{\beta}^{-1}A_{\eta_i}
	\end{bmatrix}}
\]
Moreover, because $\hat{z}$ is assumed to be a basic feasible ray, it can be represented with the following structure:
\[
\tag{E3.2.4}\label{e324}
\mathlarger{\hat{z} = \begin{bmatrix}
	\hat{z}_{\eta}\\
	\hat{z}_{\beta}
	\end{bmatrix} =
	\begin{bmatrix}
	e_j\\
	-A_{\beta}^{-1}A_{\eta_j}
	\end{bmatrix}}
\]
Substituting \ref{e322}, \ref{e323}, \ref{e324} into \ref{e321}, we obtain the following expression:
\[
\tag{E3.2.5}\label{e325}
\begin{array}{rrl}
\begin{bmatrix}
	e_j\\
	-A_{\beta}^{-1}A_{\eta_j}
	\end{bmatrix}	&	=	&
\begin{bmatrix}
	\alpha_1+\gamma_1\\
	\alpha_2+\gamma_2\\
	:\\
	:\\
	\alpha_{n-m}+\gamma_{n-m}\\
	-(\alpha_i+\beta_i)A_{\beta}^{-1}A_{\eta_i}
	\end{bmatrix}
\end{array}
\]
In order for the equality to hold in \ref{e325}, we can compare each of the $n$ equations embedded in the vector equality to observe the following:
\[
\tag{E3.2.6}\label{e326}
\begin{array}{rrrrrl}
\alpha_i&+&\gamma_i&=&0&~\forall i\neq j\\
\alpha_i&+&\gamma_i&=&1&~i=j
\end{array}
\]
Furthermore, because $z^1$ and $z^2$ are rays in the \emph{feasible} region, i.e. $z^1,z^2>\mathbf{0}$ and $z^i\geq\mathbf{0}$ by construction, we reason that $\alpha_i, \gamma_i \geq 0$ as well. Combining this conclusion with the first equation in \ref{e326} we get the following result:
\[
\tag{E3.2.7}\label{e327}
\begin{array}{rrrrrl}
\alpha_i& & & =&0&~\forall i\neq j\\
		& &\gamma_i&=&0&~\forall i\neq j\\
\alpha_j&+&\gamma_j&=&1&
\end{array}
\]
If either $\alpha_j$ or $\gamma_j$ are zero, then $\hat{z}\equiv{z^1}$ or $\hat{z}\equiv{z^2}$ respectively and we violate the assumption that $z^1,z^2\neq\mathbf{0}$, which is a contradiction. Otherwise, we observe that $\hat{z}=z^j$ and say
\[
\begin{array}{rrrrrr}
	&\hat{z}	&	=	&	z^1	&	+	&	z^2\\
\Rightarrow& z^j	&	=	&\alpha_jz^j&	+	&\gamma_jz^j\\
\Rightarrow & z^j	&	=	&\alpha_jz^j&	+	&\gamma_jz^j\\
\Rightarrow & (1-\alpha_j)z^j	&	=	&\gamma_jz^j&		& \\
\Rightarrow & (1-\alpha_j)\alpha_jz^j	&	=	&\alpha_j\gamma_jz^j&		& \\
\Rightarrow & (1-\alpha_j)z^1	&	=	&\alpha_jz^2&		& \\
\Rightarrow & z^1	&	=	&\dfrac{\alpha_j}{1-\alpha_j}z^2&		& \\
\Rightarrow & z^1	&	=	&\mu z^2&		& \\
\end{array}
\]
which is a contradiction. Therefore, we reason that the initial assumption that $\hat{z}$ can be expressed as a linear combination of two other rays $z^1$ and $z^2$ must be erroneous. Consequently, $\hat{z}$ must be an extreme ray.
\end{proof}