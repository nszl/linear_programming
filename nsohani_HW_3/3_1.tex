\exercise{3.1} \emph{Illustrate Algebraic and Geometric Concepts}\\
\noindent
Let us consider a linear optimization example conducive to algebraic and two-dimensional geometric analysis. In order to design such a problem, we posit that this problem have two more equations than constraints; such a combination allows us to project the feasible region onto the space of non-basic variables.

\bigskip
\noindent
Suppose we have a problem of six variables and four equations as given below:

\[
\tag{E3.1.1}\label{e311}
\begin{array}{rrl}
A	&	=	&	\begin{bmatrix}
				2 & 3 & 1 & 0 & 0 & 0\\
				3 & 1 & 0 & 1 & 0 & 0\\ 
				1 & 2 & 0 & 0 & 1 & 0\\
				0 & 1 & 0 & 0 & 0 & 1
				\end{bmatrix}\\
b	&	=	&	\begin{bmatrix}
				5 & 4 & 3 & 1
				\end{bmatrix}^{T}				

\end{array}
\]

\bigskip
\noindent
We can assign basic partitions to the system in \ref{e311}. In order to create a basic partition, we require four linearly independent columns of $A$; hence there is a combinatorial aspect to the basis formulation problem because we require that $A_{\beta}$ be invertible. For example, both $x_2$ and $x_6$ cannot belong to the non-basis in this problem because the remaining columns would be linearly dependent. With each basic partition, we associate a basic solution, which is the unique result to the following:

\[
A\bar{x}=A_{\beta}\bar{x}_{\beta}+A_{\eta}\bar{x}_{\eta}=b
\]

\bigskip
\noindent
We consider all permissible basic partitions and their associated basic solutions. These solutions can be categorized as being either feasible or infeasible, depending upon whether the basic variables satisfy the non-negativity constraint imposed in the primal problem. Additionally, each basic solution will have $n-m$ basic directions, which lie in the null space of $A$. For our example, the null space is two dimensional, and hence each basic solution will have two basic directions. A basic direction is termed a basic \emph{feasible} direction if its associated feasible solution remains feasible while moving along the basic direction for a sufficiently small $\epsilon$. The data associated with all permissible partitions is given below:

\bigskip

\[
\tag{Summary of Permissible Basic Partitions}
\begin{array}{cccccccc}
\beta	&	\eta	&	\bar{x}	&	{\tt Feasible}	&	\bar{A}_{\eta_1}		&	\epsilon_{\eta_1}	&	\bar{A}_{\eta_2}		&	\epsilon_{\eta_2}\\ \hline
3,4,5,6 &	1,2	&  [0~0~5~4~3~1]^T	&	{\tt Y}	&	[2~3~1~0]^T		& 	\frac{4}{3}		&	[3~1~2~1]^T	&	1\\
2,4,5,6 &	1,3	&  [0~\frac{5}{3}~0~\frac{7}{3}~-\frac{1}{3}~-\frac{2}{3}]^T	&	{\tt N}	&			&			&				&		\\
2,3,5,6 &   1,4	&  [0~4~-7~0~-5~-3]^T	&	{\tt N}	&			&			&				&		\\
2,3,4,6 &   1,5	&  [0~\frac{3}{2}~\frac{1}{2}~\frac{5}{2}~0~-\frac{1}{2}]^T	&	{\tt N}	&			&			&				&		\\
2,3,4,5 &   1,6	&  [0~1~2~3~1~0]^T		& {\tt Y}	&	[0~2~3~1]^T	&	1	&	[1~-3~-1~-2]^T	&	1	\\ \hline
1,4,5,6 &   2,3	&  [\frac{5}{2}~0~0~-\frac{7}{2}~\frac{1}{2}~1]^T	&	{\tt N}	&			&			&				&		\\
1,3,5,6 &   2,4   & [\frac{4}{3}~0~\frac{7}{3}~0~\frac{5}{3}~1]^T	&	{\tt Y}	&	[\frac{1}{3}~\frac{7}{3}~\frac{5}{3}~1]^T	&	1	&	[\frac{1}{3}~-\frac{2}{3}~-\frac{1}{3}~0]^T & 4\\
1,3,4,6 &   2,5	&  [3~0~-1~~-5~0~1]^T	&	{\tt N}	&			&			&				&		\\ \hline
1,2,5,6 & 	3,4	&  [1~1~0~0~0~0]^T		&	{\tt Y}	&	[-\frac{1}{7}~\frac{3}{7}~-\frac{5}{7}~-\frac{3}{7}]^T	&	\frac{7}{3}	&	[\frac{3}{7}~-\frac{2}{7}~\frac{1}{7}~\frac{2}{7}]^T & \frac{7}{3}\\
1,2,4,6 &  3,5	&  [1~1~0~0~0~0]^T		&	{\tt Y}	&	[2~-1~-5~1]^T	&	\frac{1}{2}	&	[-3~2~7~-2]^T	&	\frac{1}{2}\\
1,2,4,5 &	3,6	&  [1~1~0~0~0~0]^T		&	{\tt Y}	&	[\frac{1}{2}~0~-\frac{3}{2}~-\frac{1}{2}]^T	&	2	&	[-\frac{3}{2}~1~\frac{7}{2}~-\frac{1}{2}]^T	&	1\\ \hline
1,2,3,6 &	4,5	&  [1~1~0~0~0~0]^T		&	{\tt Y}	&	[\frac{2}{5}~-\frac{1}{5}~-\frac{1}{5}~\frac{1}{5}]^T	&	\frac{5}{2}	&	[-\frac{1}{5}~\frac{3}{5}~-\frac{7}{5}~-\frac{3}{5}]^T & \frac{5}{3}\\
1,2,3,5 &	4,6	&  [1~1~0~0~0~0]^T		&	{\tt Y}	&	[\frac{1}{3}~0~-\frac{2}{3}~-\frac{1}{3}]^T	&	3	&	[-\frac{1}{3}~1~-\frac{7}{3}~-\frac{5}{3}]^T & 1\\ \hline
1,2,3,4 &	5,6	&  [1~1~0~0~0~0]^T		&	{\tt Y}	&	[1~0~-2~-3]^T	&	1	&	[-2~1~1~5]^T & 1\\
\hline
\end{array}
\]

\bigskip
\noindent
Note that the basic directions are directly related to the non-basic variables and $\bar{A}_{\eta_j}$ by the following relationship:
\[
\begin{array}{c}
\bar{z}_{\eta}=e_j\\
\bar{z}_{\beta}=-\bar{A}_{\eta_j}
\end{array}
\]
and are omitted from the table to prevent clutter. The table was generated using code supplied in Appendix A.2 of the text. The following solutions are extreme points of our problem:

\[
\begin{bmatrix}
0\\0\\5\\4\\3\\1
\end{bmatrix},~
\begin{bmatrix}
0\\1\\2\\3\\1\\0
\end{bmatrix},~
\begin{bmatrix}
\frac{4}{3}\\0\\\frac{7}{3}\\0\\\frac{5}{3}\\1
\end{bmatrix},~
\begin{bmatrix}
1\\1\\0\\0\\0\\0
\end{bmatrix}.
\]

\bigskip
\noindent
There are a total of four \emph{distinct} feasible solutions since any basis including the first and second column has the solution $[1~1~0~0~0~0]^T$. These feasible solutions are the extreme points of the feasible space projected onto the space of non-basic variables. If we consider the last entry from the table above, we can partition our variables in the following way: $\{\beta,\eta\}=\{(1,2,3,4),(5,6)\}$. Here we can visualize the space of non-basic variables as being spanned by orthogonally oriented $x_5$ and $x_6$ axes. Then we can create a polygon bounded by vectors representing the following set of inequalities:
\[
\bar{A}_{\eta}x_{\eta}\leq A_{\beta}^{-1}b
\]
where the inequality constraint arises from treating the vector of basic variables $x_{\beta}$ as slack variables. Considering the last table entry in detail, we see that the set of inequalities is given by:

\[
\begin{array}{rrrrl}
x_5	& - & 2x_6	 & \leq & 5~...~(x_1)\\
	&   & x_6	& \leq & 4~...~(x_2)\\
-2x_5 & + & x_6 & \leq & 3~...~(x_3)\\
-3x_5 & + & 5x_6 & \leq & 1~...~(x_4)		
\end{array}
\]

\bigskip
\noindent
The aforementioned polygon is represented in Figure \ref{fig1}. The feasible convex set is colored cyan, while basic feasible solutions project to green points that are the vertices of the polygon. Basic infeasible solutions project to red points. Note that the redundant solution at $[1~1~0~0~0~0]^T$ makes it difficult to corroborate that all nine basic feasible solutions did in fact map to green points. It is much easier to verify that the five infeasible basic solutions mapped to five red points.

\begin{figure}
\includegraphics[width=0.8\textwidth]{pivot/3_1_Figure.eps}
\caption{Feasible set}\label{fig1}
\end{figure}

\bigskip
\noindent
Finally note that this system has no basic feasible rays because there is no ray $\hat{z} \neq 0$ of the convex feasible set $S$ such that $\hat{x}+\tau\hat{z}\in S ~\forall \hat{x} \in S, \forall \tau>0$. Intuitively, this means the polygon has edges that would be traversed if a feasible $\hat{x} \in S$ started in the set and continued along a proposed basic feasible ray. Moreover, in order to have a basic direction be a ray of the feasible region, we require that $\bar{A}_{\eta_j}\leq 0$, which is clearly violated for all our partitions in the table.

\bigskip