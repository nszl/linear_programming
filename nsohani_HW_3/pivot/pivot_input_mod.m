% pivot_input.m // Jon Lee // modified by Nauman Sohani
% data for pivoting example

A = [2 3 1 0 0 0; 3 1 0 1 0 0; 1 2 0 0 1 0; 0 1 0 0 0 1];
A= sym(A);
c = [6 7 -2 0 4 4.5]';
c=sym(c);
b = [5 4 3 1]';
b=sym(b);
beta = [1,3,5,6];
[m,n] = size(A);
eta = setdiff(1:n,beta); % lazy eta initialization 