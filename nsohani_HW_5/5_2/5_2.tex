\exercise{5.2} \emph{Duality and Complementarity with {\tt AMPL}}\\
Consider the following linear optimization problem:
\[
\tag{P}\label{2.1}
\begin{array}{rrrrrrrl}
\min & 2x & + & 3y & + & 4z & &\\
	& 5x & + & 3y &   &      & \geq & 10;\\
	&      &   & 7y & +  &  z  & \leq & 9;\\
	&      &   & y & ,  &  z  & \geq & 0;\\
\end{array}
\]\\
We can formulate the dual problem of (\ref{2.1}) by considering how dual variables $\pi,\gamma$ will operate on the constraints of (\ref{2.1}):
\[
\tag{E5.2.1}\label{2.2}
\pi(5x+3y \geq 10)+\gamma(7y+z\leq9)
\]\\
Since the primal problem is a minimization problem, the dual will be a maximization one. Now we ask the question of how we can bound (\ref{2.2}) from above and below. Since we seek to minimize the objective of (\ref{2.1}), this objective will become an \emph{upper bound} on (\ref{2.2}). Moreover, we can collect the inequalities embedded in (\ref{2.2}) and construct a lower bound which we seek to maximize. Constraints on the dual problem can be constructed by collecting coefficients of $x,y,z$ in (\ref{2.2}) and comparing them to the corresponding term in the objective function. The $\leq$ is used since the objective will become an upper bound in the dual problem:
\[
\begin{array}{rrrrrrl}
5\pi x& & &\leq&2x&~& ;\\
3\pi y& + & 7\gamma y&\leq &3y&~&y\geq0;\\
	&    & \gamma z&\leq &4z&~&z\geq0;
\end{array}
\]\\
Let us now inspect each of these expressions. For the first expression, there is no restriction placed on $x$; hence we can construct the following constraint: $5\pi=2$.

\bigskip
\noindent
For the second expression, since $y\geq0$, we can maintain the sense of the inequality and obtain a second constraint: $3\pi+7\gamma\leq3$.

\bigskip
\noindent
For the third expression, since $z\geq0$, we can maintain the sense of the inequality and obtain a third constraint: $\gamma\leq4$.

\bigskip
\noindent
In order to determine the dual objective, we re-visit (\ref{2.2}) and observe that it is bounded below by $10\pi+9\gamma$ with the following restrictions: $\pi\geq0$, $\gamma\leq0$. Therefore, we arrive at the following dual problem:
\[
\tag{D}\label{2.3}
\begin{array}{rrrrrl}
\max & 10\pi & + & 9\gamma &  & \\
	& 5\pi &  &  		&   = & 2;\\
	& 3\pi & + & 7\gamma & \leq & 3;\\
	&       &   &  \gamma  & \leq & 4;\\
	&  \pi &   &    & \geq & 0;\\
	&   &   & \gamma   & \leq & 0;\\
\end{array}
\]\\
Obviously, there are some redundancies in (\ref{2.3}). Nevertheless, there are no inconsistencies and thus we can find feasible dual solutions.

\bigskip
\noindent
(\ref{2.1}) can be solved with the following {\tt AMPL} model and run files:

\bigskip
\noindent
%{\tt 5_2.mod:}
\verbatiminput{5_2/5_2.mod}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
%{\tt 5_2.run:}
\verbatiminput{5_2/5_2.run}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
Running this program results in the following output:

\bigskip
\noindent
%{\tt 5_2.out:}
\verbatiminput{5_2/5_2.out}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
Therefore, the optimal solution to (\ref{2.1}) is $x=2, y=0, z=0$, and the corresponding dual solution to (\ref{2.3}) is $\pi = 0.4, \gamma = 0$. We can verify the optimality of these solutions by referring to the weak duality theorem, and computing the objective values:
\[
2x+3y+4z=2(2)+3(0)+4(0)=4;
\]
\[
10\pi+9\gamma=10(0.4)+9(0)=4.
\]\\
Clearly the strong optimal basis and strong duality theorems are satisfied since we have found optimal solutions. This was possible because the primal problem was feasible and bounded. Moreover, because our primal solution is basic, the primal and dual solutions should be complementary as well:
\[
\Bigg(\begin{bmatrix}2&3&4\end{bmatrix}-\begin{bmatrix}0.4&0\end{bmatrix}\begin{bmatrix}5&3&0\\0&7&1\end{bmatrix}\Bigg)\begin{bmatrix}2\\0\\0\end{bmatrix}=0
\]
\[
\begin{bmatrix}0.4&0\end{bmatrix}\Bigg(\begin{bmatrix}5&3&0\\0&7&1\end{bmatrix}\begin{bmatrix}2\\0\\0\end{bmatrix}-\begin{bmatrix}10\\9\end{bmatrix}\Bigg)=0
\]\\
Moreover, we see that the weak complementary slackness theorem is also satisfied and is another verification of the optimality of our solutions. Furthermore, since our solutions are optimal for (\ref{2.1}) and (\ref{2.3}), we are guaranteed complementarity by the strong complementary slackness theorem as is verified by the preceding calculations. Finally, we observe that we have a slackness of zero in the first constraint and a slackness of 9 in the second constraint of (\ref{2.1}). In fact, because the second basic variable is zero, we can have multiple complementary dual solutions; however, it is not necessary that each complementary dual solution is feasible. This topic is explored further in the following problem.