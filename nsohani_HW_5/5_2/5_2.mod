
# 5_2.mod
# 

var x;
var y >= 0;
var z >= 0;

minimize obj:              # CHOOSE MAX/MIN AND NAME THE OBJ. FUNCTION
   2*x + 3*y + 4*z;

subject to constraint_1: # DEFINING THE CONSTRAINT INDICES
   5*x + 3*y >= 10;

subject to constraint_2:
   7*y + z <= 9;