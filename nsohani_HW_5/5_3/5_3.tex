\exercise{5.3} \emph{Complementary Slackness}\\
Consider a primal-dual pair of a standard form problem:
\[
\tag{P}
\begin{array}{rrrl}
\min 	& c'x &	&\\
	&Ax & = & b;\\
	& x & \geq & \mathbf{0}
\end{array}
\]

\[
\tag{D}
\begin{array}{rllr}
\max	& y'b &	&\\
	&y'A & \leq & c'.\\
\end{array}
\]\\
This pair of systems is complementary if the following relationships hold:

\[
\begin{array}{crrrl}
(c_j-\hat{y}'A_{.j})\hat{x}_j & = & 0 & \forall & j\in[1,n];\\
\hat{y}_i(A_{i.}\hat{x}-b_i) & = & 0 & \forall & i\in[1,m];
\end{array}
\]\\
If we consider the trivial primal system where $\hat{x}=b=\mathbf{0}$, we observe that any $\hat{y}$ will satisfy complementarity where $\hat{y}$ need not be feasible for the dual problem. This can be illustrated with the following example:

\[
\tag{E5.3.1}\label{3.1}
\begin{array}{rrrl}
\min 	& \begin{bmatrix}1 & 1& 1\end{bmatrix}x &	&\\
	&\begin{bmatrix}2&3&4\\5&6&7\end{bmatrix}x & = & \begin{bmatrix}0\\0\end{bmatrix};\\
	& x & \geq & \mathbf{0}.
\end{array}
\]\\
Clearly the optimal solution to (\ref{3.1}) is $\hat{x} = [0 ~ 0 ~ 0]'$. Substituting this expression into the complementarity conditions above, we see that \emph{any} $\hat{y}$ will suffice. Consider $\hat{y}=[1~1~1]' \Rightarrow \hat{y}'A=[7~9~11]'>[1~1~1]'$, which violates the feasibility requirements for the dual problem.

\bigskip
\noindent
Let us consider under what conditions we can obtain a non-unique $\hat{y}$ to satisfy the complementarity requirements. We can restrict our analysis to the basic variables of $\hat{x}$ because a basic solution will satisfy $\hat{x}_{\eta}=\mathbf{0}$ and hence will satisfy complementarity on the non-basic positions. Therefore, we consider the following:

\[
\begin{array}{crrrl}
(c_j-\hat{y}'A_{.j})\hat{x}_j & = & 0 & \forall & j\in[1,m];\\
\hat{y}_i(A_{i.}\hat{x}-b_i) & = & 0 & \forall & i\in[1,m];
\end{array}
\]\\
where we assume, without loss of generality, that the first $m$ variables are the basic variables. The second equation will hold $\forall i\in[1,m]$ because of the basic solution assumption and does not have any implications on the uniqueness of $\hat{y}$. Consider now the first equation. For each $\hat{x}_j=0, j\in[1,m]$, we obtain one degree of freedom in selecting a complementary $\hat{y}$. This is because we now have the freedom to allow $c_j-\hat{y}'A_{.j}$ to be any real number, because it will multiply by $x_j=0$ and satisfy the equality. Then, by simply imposing the additional constraint $\{\hat{y}: \hat{y}'A_{.j}>c_j'\}$, we obtain a complementary $\hat{y}$ that is infeasible for the dual problem.

\bigskip
\noindent
Based on the conclusions from the preceding discussion, we consider the following problem, which has the same constraint matrix as (\ref{3.1}), but no longer has a trivial solution:

\[
\tag{E5.3.2}\label{2}
\begin{array}{rrrl}
\min 	& \begin{bmatrix}1 & 1& 1\end{bmatrix}x &	&\\
	&\begin{bmatrix}2&3&4\\5&6&7\end{bmatrix}x & = & \begin{bmatrix}2\\5\end{bmatrix};\\
	& x & \geq & \mathbf{0}.
\end{array}
\]\\
The optimal solution to this problem is $\hat{x}=[1~0~0]'$, ($\bar{c}_{\eta}=0$) which can be found by considering $\beta=(1,2)$. We can also calculate $\hat{y}'=c_{\beta}'A_{\beta}^{-1}=[-\frac{1}{3}~\frac{1}{3}]'$. Clearly, $\hat{y}$ as calculated in this way, is complementary and dual feasible. However, observe that our second basic variable $x_2$ is actually zero; therefore, we conclude that we can manufacture additional complementary $\hat{y}$:

\[
\begin{array}{crlll}
1-[y_1~y_2]\begin{bmatrix}2\\5\end{bmatrix} & = & 1 & ~ & j=1;\\
1-[y_1~y_2]\begin{bmatrix}3\\6\end{bmatrix} & = & a\in\mathbb{R} & ~ & j=2.\\
\end{array}
\]\\
To ensure that the solution is not dual feasible, we apply the constraint as discussed above to violate the feasibility criterion:
\[
\tag{E.5.2.3}\label{3}
\begin{array}{rrrrl}
2y_1&+&5y_2&=&1;\\
3y_1&+&6y_2&>&1.
\end{array}
\]\\
Clearly, one possible solution is $\hat{y}=[0~\frac{1}{5}]'$. We can explicitly demonstrate infeasibility:
\[
\hat{y}'A=\begin{bmatrix}0&\frac{1}{5}\end{bmatrix}\begin{bmatrix}2&3&4\\5&6&7\end{bmatrix}= \begin{bmatrix}1\\6/5\\7/5\end{bmatrix}>c'
\]
Hence, $\hat{x}$ is optimal, $\hat{x}$ and $\hat{y}$ are complementary but $\hat{y}$ is not feasible.