# csp.mod // Jon Lee
param K integer > 0; 
param W > 0; # width of stock rolls
param m >0; # number of widths
set WIDTHS:={1..m}; # set of widths to be cut
param d{WIDTHS} > 0; # number of each width to be cut
param w {WIDTHS} > 0; # actual widths
param nPAT integer >= 0; # number of patterns
set PATTERNS := 1..nPAT; # set of patterns
param A {WIDTHS,PATTERNS} integer >= 0;
var X {PATTERNS} integer >= 0;
# rolls cut using each pattern
minimize RollsUsed: # minimize rolls used
sum {j in PATTERNS} X[j];
subj to FillDemand {i in WIDTHS}:
sum {j in PATTERNS} A[i,j] * X[j]>= d[i];
#
# KNAPSACK SUBPROBLEM FOR CUTTING STOCK
# 
param ybar {WIDTHS} default 0.0;
var a {WIDTHS} integer >= 0;
minimize Reduced_Cost:
1 - sum {i in WIDTHS} ybar[i] * a[i];
subj to Width_Limit:
sum {i in WIDTHS} (w[i]+1) * a[i] <= W + K; #and sum {i in WIDTHS} a[i] <= K;

#subj to Width_Limit2:
#sum {i in WIDTHS} w[i]*a[i] = W and sum {i in WIDTHS} a[i] <= K+1;
