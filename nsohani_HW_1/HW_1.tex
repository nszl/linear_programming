
% IOE 510 HW1
% Nauman Sohani, 77319508, nsohani


% ----------------------------------------------------------------
% AMS-LaTeX ************************************************
% **** -----------------------------------------------------------
\documentclass{amsart}
\usepackage{graphicx,amsmath,amsthm}
\usepackage{hyperref}
\usepackage{verbatim}
\usepackage[a4paper,text={16.5cm,25.2cm},centering]{geometry}
%\usepackage{breqn}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
% ofS -------------------------------------------------------
\newtheorem{thm}{Theorem}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\theoremstyle{definition}
\newtheorem{defn}[thm]{Definition}
\theoremstyle{remark}
\newtheorem{rem}[thm]{Remark}		
\numberwithin{equation}{section}
% MATH -----------------------------------------------------------
\newcommand{\Real}{\mathbb R}
\newcommand{\eps}{\varepsilon}
\newcommand{\To}{\longrightarrow}
\newcommand{\BX}{\mathbf{B}(X)}
\newcommand{\A}{\mathcal{A}}
% ----------------------------------------------------------------
% EXERCISE ------------------------------------------------
\newcommand{\exercise}[1]{\noindent {\bf Exercise #1.}}  % Allow for correctly labeling exercises

\begin{document}

\title{IOE 510: Homework 1}

\maketitle

\noindent
Date submitted: January 18, 2016\\
Prepared by:
 \href{mailto:nsohani@umich.edu}
{Nauman Sohani (nsohani@umich.edu)}

\bigskip

% ----------------------------------------------------------------

\exercise{1.1} \emph{Convert to Standard Form}\\
\noindent
Consider the following optimization problem:
\[
\tag{E1.1.1}\label{e1.1.1}
\begin{array}{rrrrrrrrcl}
 \max & 5x_1 & + & 6x_2 & - & 3x_3 & + & 4x_4 &      &     \\
          & x_1   & + & x_2 & + & x_3 &       &        & \geq & 6~;\\
          &          &    & x_2 &    &         & - & x_4 & \geq & 5~;\\
          & x_1   &    &        &     &       &   &        & \geq & 0~;\\
          &          &    & x_2 &     &       &   &        & \geq & 0~.\\
\end{array}
\]
Note that $x_3$ and $x_4$ are unconstrained in this problem.\\
\bigskip

\noindent
To represent this problem in standard form, we require a minimization problem with equality constraints and non-negative feasible solutions. We begin by recognizing that the maximization problem can also be expressed as the negative of a minimization problem, i.e.
\[
\max 5x_1+6x_2-3x_3+4x_4 \Leftrightarrow -\min -5x_1-6x_2+3x_3-4x_4
\]
In this particular problem, there is no non-positive constraint on $x_1$ or $x_2$; therefore, each of these variables can be left with its original sense. However, to impose some non-negative restriction on the unconstrained variables, $x_3$ and $x_4$, we express each of these variables as a difference of two other variables and require that each of these variables be non-negative, i.e.
\[
x_3=x_3^+-x_3^-,\hphantom{10}x_3^+, x_3^-\geq0
\]
\[
x_4=x_4^+-x_4^-,\hphantom{10}x_4^+, x_4^-\geq0
\]
Next, the constraints can be modified to equalities by introducing surplus variables and requiring that these surplus variables be non-negative, i.e.
\[
x_1+x_2+x_3^+-x_3^-\geq6 \Rightarrow x_1+x_2+x_3^+-x_3^--s_1=6,\hphantom{10}s_1\geq0
\]
\[
x_2-(x_4^+-x_4^-)\geq5 \Rightarrow x_2-(x_4^+-x_4^-)-s_2=5,\hphantom{10}s_2\geq0
\]
The modifications made to the optimization problem posed in \ref{e1.1.1} preserved the problem itself but allowed for expressing this problem in standard form as given below.
\[
\tag{E1.1.2}\label{e1.1.2}
\begin{array}{rrrrrrrrrrrrrrrrrl}
-\min & -5x_1 & - & 6x_2 & + & 3x_3^+ & - & 3x_3^- & - & 4x_4^+ & + & 4x_4^- & & & & & & \\
          & x_1   & + & x_2 & + & x_3^+ & - & x_3^- &  &  & & & -& s_1 & & & = & 6~;\\
          &          &    & x_2 &    &          & & &- & x_4^+ & + & x_4^- &  & & -& s_2 & = & 5~;\\
          & x_1   &    &        &     &       &   &        & & & & & & & & &\geq & 0~;\\
          &          &    &  x_2 &     &       &   &        & & & & & & & & &\geq & 0~;\\
          &    &    &     &     & x_3^+ &   &        & & & & & & & & &\geq & 0~;\\
          &    &    &     &     &  &   & x_3^-       & & & & & & & & &\geq & 0~;\\
          &    &    &     &     &  &   &        & & x_4^+ & & & & & & &\geq & 0~;\\
          &    &    &     &     &  &   &  & & & & x_4^-& & & & &\geq & 0~;\\
          &    &    &     &     &  &   &  & & & & & & s_1 & & &\geq & 0~;\\
          &    &    &     &     &  &   &  & & & & & &  & & s_2 &\geq & 0~.\\         
\end{array}
\]
\ref{e1.1.2} can be expressed using matrix notation as well, as given below:
\[
\tag{E1.1.3}
\begin{array}{rrrl}
-\min & c'x  &      &   \\
      &  Ax  &   =  & b~; \\
      &   x  & \geq & \mathbf{0}~.
\end{array}
\]
where the matrix definitions are as follows, and it is understood that $\mathbf{0}$ refers to the appropriately sized 0 matrix.
\begin{equation}
\tag{E1.1.4}
	c = \begin{bmatrix}
		-5 & -6 & 3 & -3 & -4 & 4 & 0 & 0
		\end{bmatrix}^{T}
\end{equation}

\begin{equation}
\tag{E1.1.5}
	x = \begin{bmatrix}
		x_1 & x_2 & x_3^+ & x_3^- & x_4^+ & x_4^- & s_1 & s_2
		\end{bmatrix}^{T}
\end{equation}

\begin{equation}
\tag{E1.1.6}
	A = \begin{bmatrix}
		1 & 1 & 1 & -1 & 0 & 0 & -1 & 0\\
		0 & 1 & 0 & 0 & -1 & 1 & 0 & -1\\
		\end{bmatrix}
\end{equation}

\begin{equation}
\tag{E1.1.7}
	b = \begin{bmatrix}
		6 & 5
		\end{bmatrix}^{T}
\end{equation}

\bigskip

\exercise{1.2} \emph{Weak Duality Theorem}\\
\noindent
Suppose we have the following optimization problem:
\[
\tag{E1.2.1}\label{e1.2.1}
\begin{array}{rrrrrrrrcl}
 \min & 3x_1 & + & 2x_2 & - & x_3 &      &     \\
          & x_1   & + & x_2 & + & x_3 &  = & 4~;\\
          & x_1 & - & x_2 &  &  & = & 5~;\\
          &  x_1  &    &       &     &       &   \geq & 0~;\\
          &          &    & x_2 &     &       &   \geq & 0~;\\
          &          &    &  &     & x_3 &   \geq & 0~.\\
\end{array}
\]
We can represent \ref{e1.2.1} using the convenient matrix notation to obtain the following:
\[
\tag{E1.2.2}\label{e1.2.2}
\begin{array}{rrrc}
\min & \begin{bmatrix}
		3 & 2 & -1
		\end{bmatrix}x & &\\
	& \begin{bmatrix}
			1 &1 &1\\
			1&-1&0
		\end{bmatrix}x & = & \begin{bmatrix}4\\5\end{bmatrix}~;\\
	&                           x & \geq & \mathbf{0}~,
\end{array}
\]
where $x = \begin{bmatrix} x_1 & x_2 & x_3 \end{bmatrix}^{T}$. We can formulate the dual of \ref{e1.2.2} by inspection, as given below:

\[
\tag{E1.2.3}\label{e1.2.3}
\begin{array}{rlrl}
\max & y'\begin{bmatrix}
		4\\5\end{bmatrix}& &\\
	& y'\begin{bmatrix}
			1 &1 &1\\
			-1&1&0
		\end{bmatrix} & \leq & \begin{bmatrix} 3 & 2 & -1 \end{bmatrix}~,\\
\end{array}
\]
where $y = \begin{bmatrix}y_1&y_2\end{bmatrix}^{T}$. \\
\noindent
Suppose we consider feasible solutions to \ref{e1.2.2} and \ref{e1.2.3}; let \emph{\^{x}} $=\begin{bmatrix} 5&0&-1 \end{bmatrix}^{T}$ and \emph{\^{y}} $=\begin{bmatrix} -1 & 0\end{bmatrix}^{T}$. According to the Weak-Duality Theorem, the value of the objective function of \ref{e1.2.2} is greater than or equal to the objective function of a feasible solution to \ref{e1.2.3}. This can be verified by computing the values of the objective functions at these feasible solutions:
\[
\tag{Feasible solution to \ref{e1.2.2}}
\begin{bmatrix}
	3 & 2 & -1
\end{bmatrix}
\begin{bmatrix}
	5\\0\\-1
\end{bmatrix}=16
\]

\[
\tag{Feasible solution to \ref{e1.2.3}}
\begin{bmatrix}
	-1&0
\end{bmatrix}
\begin{bmatrix}
	4\\5
\end{bmatrix}=-4
\]
Clearly, this pair of feasible solutions corroborates the Weak Duality Theorem. In fact, because of the non-negative restriction on $x$ in \ref{e1.2.2}, the Weak Duality Theorem will always hold. Equality between the pair of objective functions of the optimization problems implies optimum solutions.
\bigskip

\exercise{1.3} \emph{Convert to $\leq$ Form}\\
\noindent
The three types of linear constraints for a linear optimization problem are that a real linear function be $\leq, \geq, $ or $=$ to a constant [p.1, LP2.95, J. Lee]. If a linear optimization problem consists only of $\leq$ constraints, then nothing needs to be done. If, however, a linear optimization problem consists of a combination of all three types of constrains, each of the $=$ and $\geq$ constraints can be treated individually and then recombined with the remaining constraints to form the complete set of constraints. 

\bigskip
\noindent
First, consider $=$ constraints. Equalities can be made to $\leq$ through the introduction of a surplus variable and imposing a constraint on that surplus variable as demonstrated below:
\[
x_1+x_2+...+x_n=a\Leftrightarrow x_1+x_2+...+x_n-s\leq a,\hphantom{10}s\geq0.
\]
Next, consider $\geq$ constraints. Such constraints can be made to $\leq$ by multiplying both sides of the inequality by $-1$. Multiplication by a negative reverses the sense of the inequality; an example is given below:
\[
x_1+x_2+...+x_n\geq a\Leftrightarrow -x_1-x_2-...-x_n \leq -a.
\]

\bigskip
\exercise{1.4} \emph{$m+1$ Inequalities}\\
\noindent
\begin{proof}
The system of $m$ equations in $n$ variables, $Ax=b$, can be transformed into an equivalent system of $2m$ inequalities, i.e.

\[
\tag{E1.4.1} \label{e1.4.1}
\begin{array}{ccc}
a_{11}x_1+a_{12}x_2+...+a_{1n}x_n &=& b_1~;\\
a_{21}x_1+a_{22}x_2+...+a_{2n}x_n &=& b_2~;\\
:							&:& :\\
:							&:& :\\
a_{m1}x_1+a_{m2}x_2+...+a_{mn}x_n &=& b_m~,\\
\end{array}
\]
is equivalent to 
\[
\tag{E1.4.2} \label{e1.4.2}
\begin{array}{ccc}
a_{11}x_1+a_{12}x_2+...+a_{1n}x_n &\leq& b_1~;\\
a_{21}x_1+a_{22}x_2+...+a_{2n}x_n &\leq& b_2~;\\
:							&:& :\\
:							&:& :\\
a_{m1}x_1+a_{m2}x_2+...+a_{mn}x_n&\leq& b_m~;\\
a_{11}x_1+a_{12}x_2+...+a_{1n}x_n &\geq& b_1~;\\
a_{21}x_1+a_{22}x_2+...+a_{2n}x_n &\geq& b_2~;\\
:							&:& :\\
:							&:& :\\
a_{m1}x_1+a_{m2}x_2+...+a_{mn}x_n&\geq& b_m~.\\
\end{array}
\]
\noindent
\ref{e1.4.2} results from the fact that each of the \emph{pairs} of corresponding $\leq$ and $\geq$ expressions intersects at only equality; hence a feasible solution to \ref{e1.4.2} is a feasible solution to \ref{e1.4.1} and vice versa.

\bigskip
\noindent
Now suppose we combined the $\geq$ expressions to form the following:
\[
(a_{11}+a_{21}+...+a_{m1})x_1+(a_{12}+a_{22}+...+a_{m2})x_2+...+(a_{1n}+a_{2n}+...+a_{mn})x_n\geq\sum_{i=1}^m b_i~,
\]
or, converting to $\leq$ form,
\[
\tag{E1.4.3}\label{e1.4.3}
-(a_{11}+a_{21}+...+a_{m1})x_1-(a_{12}+a_{22}+...+a_{m2})x_2-...-(a_{1n}+a_{2n}+...+a_{mn})x_n\leq-\sum_{i=1}^m b_i~.
\]
Next consider the system of the sum of the first $(m-1)$ inequalities of \ref{e1.4.2} and \ref{e1.4.3}:
\[
(a_{11}+a_{21}+...+a_{(m-1)1})x_1+(a_{12}+a_{22}+...+a_{(m-1)2})x_2+...+(a_{1n}+a_{2n}+...+a_{(m-1)n})x_n \leq b_1+b_2+...+b_{m-1}~;\\
\]
\[
-(a_{11}+a_{21}+...+a_{m1})x_1-(a_{12}+a_{22}+...+a_{m2})x_2-...-(a_{1n}+a_{2n}+...+a_{mn})x_n\leq-(b_1+b_2+...+b_m)~.
\]
Summing the two inequalities results in the following expression:
\[
\tag{E1.4.4}\label{e1.4.4}
-a_{m1}x_1-a_{m2}x_2-...-a_{mn}x_n\leq-b_m~.
\]
Combining \ref{e1.4.4} with the $m^{th}$ inequality of \ref{e1.4.2} results in the $m^{th}$ equation of \ref{e1.4.1}. Similarly, taking the sum of the two inequalities of another system of inequalities constructed using \ref{e1.4.3} and the sum of the first $m-2$ and the $m^{th}$ inequality of \ref{e1.4.2} results in the following inequality:
\[
-a_{(m-1)1}x_1-a_{(m-1)2}x_2-...-a_{(m-1)n}x_n\leq-b_{m-1}~,
\]
which can be combined with the $(m-1)^{st}$ inequality of \ref{e1.4.2} to arrive at the $(m-1)^{st}$ equation of \ref{e1.4.1}. 

\bigskip
\noindent
Clearly, this process can be continued to construct all $m$ equations of \ref{e1.4.1}. Therefore, the system posed in \ref{e1.4.1} is equivalent to the system of inequalities below:
\[
\tag{E1.4.5} \label{e1.4.5}
\begin{array}{ccc}
a_{11}x_1+a_{12}x_2+...+a_{1n}x_n &\leq& b_1~;\\
a_{21}x_1+a_{22}x_2+...+a_{2n}x_n &\leq& b_2~;\\
:							&:& :\\
:							&:& :\\
a_{m1}x_1+a_{m2}x_2+...+a_{mn}x_n&\leq& b_m~;\\
-(a_{11}+a_{21}+...+a_{m1})x_1-(a_{12}+a_{22}+...+a_{m2})x_2-...-(a_{1n}+a_{2n}+...+a_{mn})x_n & \leq & -\sum_{i=1}^m b_i~,
\end{array}
\]
which is of course a system of $m+1$ inequalities.
\end{proof}
%\[
%\tag{E1.4.4} \label{e1.4.4}
%\begin{array}{ccc}
%\begin{multlined}
%(a_{11}+a_{21}+...+a_{(m-1)1})x_1+(a_{12}+a_{22}+...+a_{(m-1)2})x_2+...+(a_{1n}+a_{2n}+...+a_{(m-1)n})x_n &\leq& b_1+b_2+...+b_{m-1}~;\\
%\end{multlined}
%-(a_{11}+a_{21}+...+a_{(m-1)1}+a_{m1})x_1-(a_{12}+a_{22}+...+a_{(m-1)2}+a_{m2})x_2-...-(a_{1n}+a_{2n}+...+a_{(m-1)n}+a_{mn})x_n\leq-(b_1+b_2+...+b_{m-1}+b_m)~.
%\end{array}
%\]


\bigskip

\exercise{1.6} \emph{Weak Duality Theorem for a Complicated Form}\\
\noindent
Consider the following optimization problem:
\[
\tag{E1.6.1}\label{e1.6.1}
\begin{array}{rrrrrl}
\min & c'x  & +  &     f'w          &                  &  \\
      &  Ax  &  +  &     Bw          &          \leq & b; \\
      &  Dx  &      &                   &          = & g; \\
\end{array}
\]
\[
x \geq \mathbf{0}~, ~ w \leq \mathbf{0}~.
\]
This problem can be converted to standard form through the introduction of a slack variable to convert the $\leq$ constraint into an equality, and using an appropriate substitution of a negative variable, $w^-=-w$ to construct the following equivalent standard form problem:
\[
\tag{E1.6.2}\label{e1.6.2}
\begin{array}{rrrl}
\min & \begin{bmatrix}c'\\-f'\\0\end{bmatrix}'\begin{bmatrix}x\\w^-\\s\end{bmatrix} & &\\
      &  \begin{bmatrix}A & -B & 1\\D & 0 & 0\end{bmatrix}\begin{bmatrix}x\\w^-\\s\end{bmatrix} & = & \begin{bmatrix}b\\g\end{bmatrix}~;\\
      &\begin{bmatrix}x\\w^-\\s\end{bmatrix} & \geq & \mathbf{0}~.
\end{array}
\]
By inspection, the dual of \ref{e1.6.2} can be given as the following:
\[
\tag{E1.6.3}\label{e1.6.3}
\begin{array}{rlllll}
\max & y'b & + & \pi'g & & \\
      &  y'A &  + & \pi'D & \leq & c'~;\\
      &  y'B &     &         & \geq & f'~;\\
      &  y'	&	&	& \geq & 0~.
\end{array}
\]
\begin{thm}\label{wdt}
If $\hat{x}$, $\hat{w}$, and $\hat{s}$ are feasible in \ref{e1.6.2}, and $\hat{y}$ and $\hat{\pi}$ are feasible in \ref{e1.6.3}, then $c'\hat{x} + f'\hat{w}\geq \hat{y}b+\hat{\pi}g$.
\end{thm}
\begin{proof}
Let $\mathbf{\hat{x}}$ be the vector of feasible solutions to \ref{e1.6.2}, and let $\mathbf{\hat{y}}$ be the vector of feasible solutions to \ref{e1.6.3}.

\bigskip

\noindent
If $\mathbf{\hat{y}}$ is a feasible solution, then $\mathbf{\hat{y}}'\begin{bmatrix}A & -B & 1\\D & 0 & 0\end{bmatrix} \leq \begin{bmatrix}c' & -f' & 0\end{bmatrix}$. We can left multiply both sides of the equation by $\mathbf{\hat{x}}$ without changing the sense of the inequality because $\mathbf{\hat{x}}\geq0$ since it is a feasible solution as well. The result is given as:
\[
\mathbf{\hat{y}}'\begin{bmatrix}A & -B & 1\\D & 0 & 0\end{bmatrix}\mathbf{\hat{x}} \leq \begin{bmatrix}c' & -f' & 0\end{bmatrix}\mathbf{\hat{x}}
\]
\[
\Rightarrow \mathbf{\hat{y}}'\begin{bmatrix}b\\g\end{bmatrix} \leq \begin{bmatrix}c' & -f' & 0\end{bmatrix}\mathbf{\hat{x}}
\]
\[
\Rightarrow \hat{y}b+\hat{\pi}g \leq c'\hat{x} + f'\hat{w}
\]
\[
\Rightarrow c'\hat{x} + f'\hat{w}\geq \hat{y}b+\hat{\pi}g
\]
\end{proof}
\end{document}
% ----------------------------------------------------------------


