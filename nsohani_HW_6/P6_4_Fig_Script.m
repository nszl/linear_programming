% This script will plot the changes in objective function as individual
% b[i] are varied from negative infinity to positive infinity.

clc;
clear;

min = -70;
max = 30;

b1 = linspace(min,max,201);
b2 = linspace(min,max,201);
b3 = linspace(min,max,201);

fb1 = zeros(1,length(b1));
fb2 = zeros(1,length(b2));
fb3 = zeros(1,length(b3));

for j = 1:length(b1)
   if b1(j)<=-64
      fb1(j) = -57/20*b1(j)-92/5;
   
   elseif b1(j)>-64 && b1(j)<=3.5
      fb1(j) = -7/3*b1(j)+44/3;
   
   elseif b1(j)>3.5 && b1(j)<=6
      fb1(j) = 9/5*b1(j)+1/5;
   
   elseif b1(j)>6
      fb1(j) = 8*b1(j)-37;
   end 
end

for j = 1:length(b2)
   if b2(j)<=-2
       fb2(j) = -8/3*b2(j)+11/3;
   
   elseif b2(j)>-2
       fb2(j) = 5/2*b2(j)+14;
   end
end

for j = 1:length(b3)
    if b3(j)<=14
        fb3(j) = -7/2*b3(j)+61;
   
    elseif b3(j)>14 && b3(j)<=19
        fb3(j) = -2/5*b3(j)+88/5;
    
    elseif b3(j)>19
        fb3(j) = 5/3*b3(j)-65/3;
    end
end

figure(1)
plot(b1,fb1,'b-',b2,fb2,'g--',b3,fb3,'r:');
title('$b_i$ vs. objective value','interpreter','latex','fontsize',14);
ylabel('objective value','interpreter','latex','fontsize',14);
legend({'$f(b_1)$','$f(b_2)$','$f(b_3)$'},'interpreter','latex',...
    'fontsize',14);
set(findall(gcf,'type','line'),'linewidth',2);
grid on;