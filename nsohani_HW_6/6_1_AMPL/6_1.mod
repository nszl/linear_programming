# 6_1.mod
# 

var x1 >= 0;
var x2 >= 0;
var x3 >= 0;

minimize obj:              # CHOOSE MAX/MIN AND NAME THE OBJ. FUNCTION
   x1 + x2 + 1.0001*x3;

subject to constraint_1: # DEFINING THE CONSTRAINT INDICES
   2*x1 + 3*x2 + 4*x3 = 8;

subject to constraint_2:
   5*x1 + 6*x2 + 7*x3 = 17;