\exercise{6.1} \emph{Illustrate local sensitivity analysis}\\
Consider a primal-dual pair of a standard form problem:
\[
\tag{P}
\begin{array}{rrrl}
\min 	& c'x &	&\\
	&Ax & = & b;\\
	& x & \geq & \mathbf{0}
\end{array}
\]

\[
\tag{D}
\begin{array}{rllr}
\max	& y'b &	&\\
	&y'A & \leq & c'.\\
\end{array}
\]\\
We define the function $f:\mathbb{R}^m\rightarrow\mathbb{R}$ as the optimal objective value viewed as a function of its right-hand side vector $b$. Let us consider the following example in detail:

\[
\tag{E6.1.1}\label{1.1}
\begin{array}{rrrl}
\min 	& \begin{bmatrix}1 & 1& 1\end{bmatrix}x &	&\\
	&\begin{bmatrix}2&3&4\\5&6&7\end{bmatrix}x & = & \begin{bmatrix}8\\17\end{bmatrix};\\
	& x & \geq & \mathbf{0}.
\end{array}
\]\\
For this example, we claim that the optimal basis is $\beta = (1,2)$, and the basic solution corresponding to the optimal basis is $\bar{x}=\begin{bmatrix}1&2\end{bmatrix}^T$. In order to verify this claim, we compute the following matrices associated with this basic partition:

\[
A_{\beta}=\begin{bmatrix}2&3\\5&6\end{bmatrix}\Rightarrow A_{\beta}^{-1}=\begin{bmatrix}-2&1\\5/3&-2/3\end{bmatrix}
\]\\
Therefore, the basic solution can be computed as:
\[
\bar{x}=A_{\beta}^{-1}b=\begin{bmatrix}-2&1\\5/3&-2/3\end{bmatrix}\begin{bmatrix}8\\17\end{bmatrix}=\begin{bmatrix}1\\2\end{bmatrix}
\]\\
Optimality of this solution can be verified by computing the reduced cost associated with the non-basic variables: 
\[
\bar{c}_{\eta} = c_{\eta}'-c_{\beta}'A_{\beta}^{-1}A_{\eta}=1-\begin{bmatrix}1&1\end{bmatrix}\begin{bmatrix}-2&1\\5/3&-2/3\end{bmatrix}\begin{bmatrix}4\\7\end{bmatrix}=1-1=0.
\]\\
Hence, we can achieve no reduction in our objective by performing a pivot to another basis and our proposed basis is optimal. Consequently, we can compute $\bar{y}'=c_{\beta}'A_{\beta}^{-1}=\begin{bmatrix}-1/3&1/3\end{bmatrix}$. 

\bigskip
\noindent
Next we can define $\mathcal{B}:=\{b:A_{\beta}^{-1}b\geq0\}$, which refers to the set of right-hand side vectors $b$ such that $\beta$ remains an optimal basis. Under the optimality hypothesis, the objective values of the primal and dual problems are identical and hence we can also express $f(b)=\bar{y}'b$, which is a linear function on $b\in\mathcal{B}$ where the requirement that $b\in\mathcal{B}$ arises from the optimality criterion. Therefore, we conclude that $\bar{y}$ is the gradient of $f$ and we can predict, at least locally, how the objective function will change as a consequence of changes to $b$. This relationship can be made more explicit by expressing $f$ in the following way:

\[
\tag{E6.1.2}\label{e1.2}
f(b)=c'\bar{x}=\bar{y}'b=-\frac{1}{3}b_1+\frac{1}{3}b_2
\]\\
The next consideration is how much change we can tolerate in $b$ while satisfying the feasibility and optimality condition for $\beta$. Consider the following amendment to the primal problem as formulated above:

\[
\tag{P$_i$}
\begin{array}{rrrl}
\min 	& c'x &	&\\
	&Ax & = & b+\Delta_ie^i;\\
	& x & \geq & \mathbf{0}
\end{array}
\]\\
In order to ensure optimality, we require that $A_{\beta}^{-1}(b+\Delta_ie^i) \geq 0 \Rightarrow L_i\leq\Delta_i \leq U_i$ where
\[
\mathlarger{L_i = \underset{k: h_k^i>0}{\operatorname{max}}\{-\frac{A_{\beta}^{-1}b_k}{A_{\beta}^{-1}e^i_k}\}}
\]
\[
\mathlarger{U_i = \underset{k: h_k^i<0}{\operatorname{min}}\{-\frac{A_{\beta}^{-1}b_k}{A_{\beta}^{-1}e^i_k}\}}
\]\\
Using these formulations, we can construct limits on $\Delta_i$ for $i=1,2$:

\[
\begin{array}{rl}
i=1: 	& \\
	& A_{\beta}^{-1}e^1 = \begin{bmatrix}-2\\5/3 \end{bmatrix}\\
\Rightarrow & \mathlarger{L_1 = -\frac{2}{5/3}=-\frac{6}{5}}\\
\Rightarrow & \mathlarger{U_1 = -\frac{1}{-2}=\frac{1}{2}}\\	
\end{array}
\]

\[
\begin{array}{rl}
i=2: 	& \\
	& A_{\beta}^{-1}e^2 = \begin{bmatrix}1\\-2/3 \end{bmatrix}\\
\Rightarrow & \mathlarger{L_2 = -\frac{1}{1}=-1}\\
\Rightarrow & \mathlarger{U_2 = -\frac{2}{-2/3}=3}\\	
\end{array}
\]\\
Let us verify that these limits actually hold as we change one right-hand side element at a time. First we consider the lower and upper limits pertaining to $i=1$:

\[
b^{1L}=b+\begin{bmatrix}-1.2\\0\end{bmatrix}=\begin{bmatrix}6.8\\17\end{bmatrix}\Rightarrow \bar{x}^{1L}=\begin{bmatrix}3.4\\0\end{bmatrix}
\]
\[
b^{1U}=b+\begin{bmatrix}0.5\\0\end{bmatrix}=\begin{bmatrix}8.5\\17\end{bmatrix}\Rightarrow \bar{x}^{1U}=\begin{bmatrix}0\\2.833\end{bmatrix}
\]\\
We repeat the analysis for $i=2$:
\[
b^{2L}=b+\begin{bmatrix}0\\-1\end{bmatrix}=\begin{bmatrix}8\\16\end{bmatrix}\Rightarrow \bar{x}^{2L}=\begin{bmatrix}0\\2.667\end{bmatrix}
\]
\[
b^{2U}=b+\begin{bmatrix}0\\3\end{bmatrix}=\begin{bmatrix}8\\20\end{bmatrix}\Rightarrow \bar{x}^{2U}=\begin{bmatrix}4\\0\end{bmatrix}
\]\\
Evidently, we conclude that $\mathcal{B}=\{b+\Delta_ie^i:L_i\leq\Delta_i \leq U_i\}$, where $L_i$ and $U_i$ are as defined above for $i=1,2$. 

\bigskip
\noindent
Furthermore, we can assess how $f(b)$ changes as a result of changes to $b$. As described in \ref{e1.2}, changes to the first entry in $b$ will cause $f(b)$ to change by $-1/3$ per unit change in $b_1$. Therefore, by changing $b$ to $b^{1L}$, we expect a change in the objective of $(-1.2)(-1/3)=0.4$, which is verified by computing the new objective value corresponding to $\bar{x}^{1L}$ (3.4) and comparing with the original objective value (3). Similarly, changing $b$ to $b^{1U}$ causes a change of $-0.167$ (objective: $3\rightarrow2.833$); changing $b$ to $b^{2L}$ causes a change of $-0.333$ (objective: $3\rightarrow2.667$); changing $b$ to $b^{2U}$ causes a change of $1$ (objective: $3\rightarrow4$). 

\bigskip
\noindent
Next consider the function $g:\mathbb{R}^n\rightarrow\mathbb{R}$ which is the optimal objective value viewed as a function of its objective vector $c$. Here we define $\mathcal{C}$ as the set of objective vectors such that $\beta$ remains an optimal basis. The criterion for optimality is the same as that of the Simplex algorithm and indeed the same verification we did above: $\mathcal{C}=\{c:c_{\eta}'-c_{\beta}'A_{\beta}^{-1}A_{\eta}\geq0\}$.

\bigskip
\noindent
For this example, we can develop the limits on the changes to the objective function by perturbing each of the objective vector entries:

\[
\begin{array}{rrrl}
	& (1+\Delta_{\eta})-\begin{bmatrix}1+\Delta_1&1+\Delta_2\end{bmatrix}\begin{bmatrix}-2&1\\5/3&-2/3\end{bmatrix}\begin{bmatrix}4\\7\end{bmatrix} & \geq & 0\\
\Rightarrow & (1+\Delta_{\eta})-\begin{bmatrix}1+\Delta_1&1+\Delta_2\end{bmatrix}\begin{bmatrix}-1\\2\end{bmatrix} & \geq & 0\\
\Rightarrow & 1+\Delta_{\eta}+1+\Delta_1-2-2\Delta_2 & \geq & 0\\
\Rightarrow & \Delta_{\eta}+\Delta_1-2\Delta_2 & \geq & 0
\end{array}
\]\\
If we only adjust one entry at a time, we find the following limits to ensure that $\beta$ remains the optimal basis:

\[
\Delta_{\eta}\in[0, +\infty) \Rightarrow c_{\eta}\in [1, +\infty)
\]
\[
\Delta_1\in[0, +\infty) \Rightarrow c_{1}\in [1, +\infty)
\]
\[
\Delta_2\in(-\infty, 0] \Rightarrow c_{2}\in (-\infty, 1]
\]\\
Moreover, we can express $g$ explicitly as the following:

\[
\tag{E6.1.3}\label{e1.3}
g(c)=c_{\beta}'\bar{x}_{\beta}=c_1+2c_2
\]\\
In this form, it is evident how the objective function will change as a result of changes to the objective vector, provided that the perturbations satisfy the requirements above.

\bigskip
\noindent
Local sensitivity analysis can also be explored using the following {\tt AMPL} model and run files.

\bigskip
\noindent
%{\tt 5_2.mod:}
\verbatiminput{6_1_AMPL/6_1.mod}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
%{\tt 5_2.run:}
\verbatiminput{6_1_AMPL/6_1.run}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
Running this program results in the following output:

\bigskip
\noindent
%{\tt 5_2.out:}
\verbatiminput{6_1_AMPL/6_1.out}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
Observe that the resulting output corroborates the preceding analysis. Note that the minor numerical discrepancies arise because the objective function in model file is slightly modified to force the solver to choose the same optimal basis used for this analysis. Otherwise, the solver can opt for selecting $\beta=(1,3)$, which is also a valid optimal basis. In particular the very large numbers appearing in the limits on the objective function are essentially infinity as shown in our analysis.

\bigskip