# 6_2_nominal.mod
# 

var x1 >= 0;
var x2 >= 0;
var x3 >= 0;
var x4 >= 0;
var x5 >= 0;

minimize obj:              # CHOOSE MAX/MIN AND NAME THE OBJ. FUNCTION
   1*x1 + 4*x2 + 5*x3 + 2*x4 + 3*x5;

subject to constraint_1: # DEFINING THE CONSTRAINT INDICES
   1*x1 + -1*x2 + 0*x3 + -1*x4 + 0*x5 = 7;

subject to constraint_2:
   0*x1 + -5*x2 + 2*x3 + 2*x4 + 1*x5 = 2;

subject to constraint_3:
   2*x1 + -7*x2 + 0*x3 + 3*x4 + 3*x5 = 12;