% pure_gomory_input.m // Jon Lee
% data for pivoting example

A = [14 16 -4 2 3; 5 6 -1 2 1];
A= sym(A);
c = [130 135 -20 5 70]';
c=sym(c);
b = [30 25]';
b=sym(b);
beta = [1,2];
[m,n] = size(A);
eta = setdiff(1:n,beta); % lazy eta initialization