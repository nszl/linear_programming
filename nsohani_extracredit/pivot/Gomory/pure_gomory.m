% pure_gomory.m // Jon Lee
% append a primal column corresponding to the pure gomory cut
% syntax is pure_gomory(i), where we choose i so that ybar(i) is fractional

function pure_gomory(i)
global A c A_beta ybar m n beta eta;

ei = zeros(m,1); ei(i)=1; % ei is the i-th standard unit column
hi = linsolve(A_beta,ei); % i-th column of basis inverse
r =  -floor(hi);          % best choice of r
btilde = ei + A_beta*r;   % new column for P
A = [A btilde];
c = [c; floor(ybar'*btilde)];

eta = setdiff(1:n+1, beta);
n = n+1;
display('*** PROBABLY WANT TO APPLY pivot_algebra! ***')






