%% Phase I Problem
% This function will solve the Phase I problem
clc;
clear;

pivot_setup;

Atemp = A;

An1 = -A(:,beta)*ones(length(beta),1);

A = [A An1];
% remember to restore A

eta = [eta size(A,2)];
% remember to restore eta after phase I problem

ctemp = c;

c = [zeros(size(A,2)-1,1); 1];
% remember to restore c

n = n+1;
% remember to decrement n

pivot_algebra;

[minval_beta, beta_star] = min(xbar_beta);

if(minval_beta) < 0
    pivot_swap(length(eta),beta_star);
    pivot_algebra;
end

%% Worry-free Simplex Algorithm for Phase I

% This function will run the worry-free Simplex algorithm, i.e. ignoring
% degeneracy and cycling.

while(obj > 0)
    pivot_algebra;
    
    % Step 1: Check if cbar_eta >=0; if yes, then stop: xbar and ybar are
    % optimal and we are done; otherwise continue
    [minval_cbar_eta, eta_j] = min(cbar_eta);

    if(minval_cbar_eta)>=0
       % disp('cbar_eta >= 0; xbar and ybar are optimal.');
       break;
    end

    % Step 2: Choose a non-basic index eta_j with cbar_eta_j < 0
    % Use eta_j as defined above since it corresponds to the most minimal entry
    % in cbar_eta

    % Step 3: If Abar_eta_j<=0 then stop, P is unbounded and D is infeasible
    maxAbar_eta = max(Abar_eta,[],1);
    if maxAbar_eta(eta_j) <=0 
        % disp('(P) is unbounded and (D) is infeasible.');
        break;
    end

    % Step 4: determine the index corresponding to the min lambda
    %[eta_j,~] = find(cbar_eta==max(cbar_eta(cbar_eta<0)));
    
    Abar_eta_j = Abar_eta(:,eta_j);

    ratio = xbar_beta./Abar_eta_j;

    [istar,~] = find(ratio==min(ratio(ratio>0)));

    pivot_swap(eta_j,istar);

end

% ensure that system is feasible:
if obj~=0
    %disp('(P) is infeasible');
    output_file = fopen('simplex_output.out','w');
    fprintf(output_file,'(P) is infeasible. Terminating algorithm.\n');
    fprintf(output_file,'Phase I objective value is %s.',obj);
    fclose(output_file);
   return; 
end

% Now we have a feasible basis
%disp('Proceeding to Simplex algorithm for Phase II problem');
output_file = fopen('simplex_output.out','w');
fprintf('Feasible basis found; proceeding to Phase II.\n');

%% Simplex on original problem
% revert to original problem:
c = ctemp;

A = Atemp;

n = n - 1;

eta = setdiff(1:n,beta);
pivot_algebra;

% run simplex algorithm using original data with feasible basis
% check if ybar is an integer solution
while(max(logical(mod(ybar,1)>zeros(length(ybar),1))))  

    while(1)
         pivot_algebra;

        % Step 1: Check if cbar_eta >=0; if yes, then stop: xbar and ybar are
        % optimal and we are done; otherwise continue
        [minval_cbar_eta, eta_j] = min(cbar_eta);

        if(minval_cbar_eta)>=0
           %disp('cbar_eta >= 0; xbar and ybar are optimal.');
           output_file = fopen('simplex_output.out','w');
           fprintf(output_file, 'cbar_eta = %s\n',cbar_eta);
           fprintf(output_file,'xbar = %s\n', xbar);
           fprintf(output_file,'obj = %s\n',obj);
           break;
        end

        % Step 2: Choose a non-basic index eta_j with cbar_eta_j < 0
        % Use eta_j as defined above since it corresponds to the most minimal entry
        % in cbar_eta

        % Step 3: If Abar_eta_j<=0 then stop, P is unbounded and D is infeasible
        maxAbar_eta = max(Abar_eta,[],1);
        if maxAbar_eta(eta_j) <=0 
            %disp('(P) is unbounded and (D) is infeasible.');
            output_file = fopen('simplex_output.out','w');
            fprintf(output_file, '(P) is unbounded and (D) is infeasible.\n');
            fprintf(output_file, 'zbar = %s\n',pivot_direction(eta_j));
            break;
        end

        % Step 4: determine the index corresponding to the min lambda       
        Abar_eta_j = Abar_eta(:,eta_j);

        ratio = xbar_beta./Abar_eta_j;

        [istar,~] = find(ratio==min(ratio(ratio>0)));

        pivot_swap(eta_j,istar);

    end
    
    % find which index of ybar is not an integer
    intcheck = mod(ybar,1);
    
    % choose the index of intcheck that is nonzero
    [gomorypivot,~] = find(intcheck~=0);
    
    if gomorypivot > 0
        % append new column to A
        pure_gomory(gomorypivot(1));
    end
     
end

% Achieved integer solution
disp('done!')
fprintf(output_file, '\nInteger solution found.\n');
fprintf(output_file, 'ybar = %s\n',ybar);

fclose(output_file);