\exercise{8.1} \emph{Task scheduling, continued}\\
In Exercise 2.5, we formulated the task scheduling problem in the following way:
\[
\tag{E2.5.1}\label{e2.5.1}
\begin{array}{rrrrrll}
\min & t_{n+1} & & & & & \\
	& t_i & + & d_{ij} & \leq & t_j & \forall i,j\\
	& t_i &   &          & \geq & 0 & \forall i
\end{array}
\]\\
where $d_{ij}$ captures precedences as required for a given task $i$. The following example was proposed:
\[
\tag{8.1.1}\label{t8.1}
\begin{array}{c|c}
\text{Task arc}~ (i,j)	&	d_{ij}		\\	\hline
(0,1)		&	0		\\
(1,2)		&	3		\\
(1,3)		&	3		\\
(2,4)		&	2		\\
(3,4)		&	5		\\
(4,5)		&	7		\\
\end{array}
\]\\
(\ref{t8.1}) can be incorporated with the model given in (\ref{e2.5.1}) to construct the following problem. Observe that each constraint in (\ref{e812}) is just an explicit representation of the constraints proposed in the model.
\[
\tag{8.1.2}\label{e812}
\begin{array}{rrrrrl}
\min & t_5 & & & & \\
	&t_0 & + & 0 & \leq & t_1\\
	&t_1 & + & 3 & \leq & t_2\\
	&t_1 & + & 3 & \leq & t_3\\
	&t_2 & + & 2 & \leq & t_4\\
	&t_3 & + & 5 & \leq & t_4\\
	&t_4 & + & 7 & \leq & t_5\\
	&  &  & t_i & \geq & 0 ~\forall i\\	
\end{array}
\]\\
(\ref{e812}) can be rearranged and simplified to a compact representation using an extremely sparse matrix $A$. Moreover, we can assign dual variables to each of the constraints given in (\ref{e813}).
\[
\tag{P}\label{e813}
\begin{array}{rrrl|c}
\min & t_5 &   & &\text{dual variables}\\
	&\begin{bmatrix}1&-1&0&0&0&0 \\ 0&1&-1&0&0&0 \\ 0&1&0&-1&0&0 \\ 0&0&1&0&-1&0 \\ 0&0&0&1&-1&0 \\ 0&0&0&0&1&-1 & \end{bmatrix} \begin{bmatrix} t_0  \\t_1\\t_2\\t_3\\t_4\\t_5\end{bmatrix} & \leq & \begin{bmatrix}0\\-3\\-3\\-2\\-5\\-7 \end{bmatrix} & \begin{bmatrix} y_1'\\y_2'\\y_3'\\y_4'\\y_5'\\y_6' \end{bmatrix}\\
	& t_i & \geq & 0~\forall i & \\
\end{array}
\]\\
We proceed to write the dual of (\ref{e813}) below.
\[
\tag{D}\label{e814}
\begin{array}{rlcll}
\max & -3y_2-3y_3-2y_4-5y_5-7y_6 &  & & \\
	&\begin{bmatrix}1&0&0&0&0&0 \\ -1&1&1&0&0&0 \\ 0&-1&0&1&0&0 \\ 0&0&-1&0&1&0 \\ 0&0&0&-1&-1&1 \\ 0&0&0&0&0&-1 \end{bmatrix} & \begin{bmatrix} y_1  \\y_2\\y_3\\y_4\\y_5\\y_6\end{bmatrix} & \leq & \begin{bmatrix}0\\0\\0\\0\\0\\1 \end{bmatrix} \\
	& & y_i & \leq & 0~\forall i \\
\end{array}
\]\\
We observe the structure of the constraint matrix for (\ref{e814}), which is given by $A'$, is precisely a network matrix. In particular, the arcs of the network $\{e_j\}_1^6$, which are given along the columns show the directions of the flow between the nodes of the network $\{v_i\}_1^6$, which are given along the rows. The network is represented graphically in Figure \ref{fig1}. The net supply at a given node is given in parentheses beside the node label. Interestingly, we observe that $v_6$ is the only node with a nonzero net supply of $1$. By solving the dual problem using {\tt AMPL}, we can determine the amount of flow over each arc. Note that the \emph{nonpositive} constraint in (\ref{e814}) effectively reverses the sense of each of the arcs while keeping the magnitude of the dual values.

\begin{figure}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3cm,
                    thick,main node/.style={circle,draw,font=\sffamily\Large\bfseries}]
  \node[main node] (1) {$v_1~(0)$};
  \node[main node] (2) [below right of=1] {$v_2~(0)$};
  \node[main node] (3) [below of =2] {$v_3~(0)$};
  \node[main node] (4) [below left of=3] {$v_4~(0)$};
  \node[main node] (6) [below left of=1] {$v_6~(1)$};
  \node[main node] (5) [below of =6] {$v_5~(0)$};

  \path[every node/.style={font=\sffamily\small}]
    (1) edge node [right] {$e_1$} (2)
    (2) edge node [right] {$e_2$} (3)
    (2) edge node [left] {$e_3$} (4)
    (3) edge node [above] {$e_4$} (5)
    (4) edge node [right] {$e_5$} (5)
    (5) edge node [right] {$e_6$} (6);        
\end{tikzpicture}
\caption{Network Graph for the Dual Task Scheduling Problem}\label{fig1}
% adapted from source: https://tex.stackexchange.com/questions/45734/drawing-graphs-in-latex
\end{figure}

\bigskip

\noindent
This problem can be solved using the following {\tt model}, {\tt data}, and {\tt run} files.\\
\rule{\textwidth}{1pt}

\bigskip
\noindent
{\tt schedule.mod:}
\verbatiminput{8_1/schedule.mod}
\rule{\textwidth}{1pt}

\bigskip
\noindent
{\tt schedule.dat:}
\verbatiminput{8_1/schedule.dat}
\rule{\textwidth}{1pt}

\bigskip
\noindent
{\tt schedule.run:}
\verbatiminput{8_1/schedule.run}
\rule{\textwidth}{1pt}

\bigskip
\noindent
Executing this program results in the output given in {\tt schedule.out:}

\bigskip
\noindent
{\tt schedule.out:}
\verbatiminput{8_1/schedule.out}
\rule{\textwidth}{1pt}

\bigskip
\noindent
The dual solution is given by $\begin{bmatrix} 0&0&-1&0&-1&-1\end{bmatrix}$, which evidently satisfies (\ref{e814}). From Exercise 2.5, we found the optimal policy to be to initiate task 1, then complete tasks 2 and 3 in parallel and then complete task 4 once the longer of the two is completed. Finally, we can complete task 5. The dual solution seems to work backwards from the end and selects those arcs which dictate the overall length of the process thereby essentially identifying the rate-limiting steps. A simplified network diagram is given in Figure \ref{fig2}.

\begin{figure}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3cm,
                    thick,main node/.style={circle,draw,font=\sffamily\Large\bfseries}]
  \node[main node] (1) {$v_1~(0)$};
  \node[main node] (2) [below right of=1] {$v_2~(0)$};
  \node[main node] (3) [below of =2] {$v_3~(0)$};
  \node[main node] (4) [below left of=3] {$v_4~(0)$};
  \node[main node] (6) [below left of=1] {$v_6~(1)$};
  \node[main node] (5) [below of =6] {$v_5~(0)$};

  \path[every node/.style={font=\sffamily\small}]
    %(1) edge node [right] {$e_1$} (2)
    %(2) edge node [right] {$e_2$} (3)
    (2) edge node [left] {$e_3 = -1$} (4)
    %(3) edge node [above] {$e_4$} (5)
    (4) edge node [right] {$e_5 = -1$} (5)
    (5) edge node [right] {$e_6 = -1$} (6);        
\end{tikzpicture}
\caption{Simplified Network Graph for the Dual Task Scheduling Problem}\label{fig2}
% adapted from source: https://tex.stackexchange.com/questions/45734/drawing-graphs-in-latex
\end{figure}

\bigskip