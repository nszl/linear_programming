\exercise{4.1} \emph{Illustrate aspects of the Simplex Algorithm}\\
\noindent
In this problem, we incorporate the scripts presented in Appendix A.2 of the text to construct a function that carries out the Simplex algorithm. The basic development of these scripts is outlined below:

\section*{Phase I Problem}
\noindent
To initiate the Simplex algorithm, we require a basic feasible solution. While a solution may be obtained by simply guessing a basis, the Phase I problem approach codifies a more systematic method. We first consider the following linear optimization problem:
\[
\tag{$\Phi$}\label{phi}
\begin{array}{rrrrrl}
\min & 	&	& x_{n+1} &	&	\\
	& Ax & +  & A_{n+1}x_{n+1} & = & b; \\
	& x	& , 	& x_{n+1}	& \geq & \mathbf{0}.
\end{array}
\]
\noindent
$A$ and $x$ in (\ref{phi}) are the constraint matrix and variables of the primal problem. (\ref{phi}) can be solved with one of two results: the objective function is either greater than or equal to zero; this happens because of the constraint on $x_{n+1}$. If the objective of (\ref{phi}) is greater than zero, then we conclude that the primal problem is infeasible. Otherwise, $x_{n+1}=0$ and we can relegate it to the set of non-basic variables and use the resulting basic partition to begin the Simplex algorithm on the primal problem.

\bigskip
\noindent
As advised in problem statement from the text, we do not concern ourselves with degeneracy or anti-cycling in solving (\ref{phi}). Note that these concerns could be addressed by applying an algebraic perturbation and then either terminating the Phase I problem when $x_{n+1}$ was a candidate to leave the basis or allowing the algebraically perturbed problem to continue to its conclusion.

\bigskip
\noindent
Because the Simplex algorithm is required to solve the Phase I problem, we proceed to develop a script to carry out this algorithm and then return to the Phase I problem with some measures implemented to obtain a basic feasible solution.

\section*{Simplex Algorithm}
\noindent
The Simplex algorithm considers the following standard form problem:
\[
\tag{P}\label{p}
\begin{array}{rrrl}
\min & c'x	&	&\\
	& Ax & = & b; \\
	& x	&\geq & \mathbf{0}.
\end{array}
\]
\noindent
In order to initiate the Simplex algorithm, we assume that a basic feasible partition has already been selected. Once such a partition is available, the remainder of the Simplex algorithm can be summarized below:

\bigskip

\begin{enumerate}
	\item Check the values associated with the non-basic variables in the vector of reduced costs $\bar{c}$. If all of $\bar{c}_{\eta}\geq0$, that means no improvement to the objective function can be achieved by moving along this vector. Hence, the current solution is optimal and the algorithm terminates.
	\item Otherwise, we choose one of the non-basic indices where $\bar{c}_{\eta}<0$. Moving along this direction would result in an improved objective.
	\item If $\bar{A}_{\eta_j}\leq0$, then we have an unbounded solution because the objective could decrease indefinitely as $\bar{x}_{new}=\bar{x}_{old}+\lambda\bar{z}$ where $\bar{z}=[-\bar{A}_{\eta_j}, e_j]'$. Clearly $\bar{z}$ is an extreme ray. Hence the (\ref{p}) is unbounded and its dual is infeasible and the algorithm terminates.
	\item Otherwise, we determine which basic variable should leave the basis to be replaced by the $j^{th}$ non-basic variable. This is determined by finding the index corresponding to the minimum allowable $\lambda$ as described in the previous step. This process can be summarized as finding $i^*$:
	\[
	i^*=\underset{i: \bar{a}_{i,\eta_j}}{\operatorname{argmin}} \left\{\frac{\bar{b_i}}{\bar{a}_{i,\eta_j}}\right\}
	\]
	and making the following changes to the basis and non-basis:
	\[
	\beta_{new}=\{\beta_1,...,\beta_{i^*-1},\eta_j,\beta_{i^*+1},...,\beta_m\}
	\]
		\[
	\eta_{new}=\{\eta_1,...,\eta_{j-1},\beta_{i^*},\eta_{j+1},...,\eta_{n-m}\}
	\]
	\item Go to step (1).
\end{enumerate}

\bigskip

\noindent
The Simplex algorithm as described here can be used to solve (\ref{phi}) to obtain a feasible basis. Then, (\ref{p}) can be solved using the basis determined from Phase I for the first algorithm iteration. This process is implemented in the following {\tt MATLAB} code:

\bigskip
\noindent
{\tt HW\_4\_MATLAB\_P1.m}:
\lstinputlisting{HW_4_MATLAB_P1.m}
\begin{center}
\line(1,0){400}
\end{center}
\bigskip

\noindent
Note that the matrices belonging to the original problem are stored in temporary variables and then restored after solving the Phase I problem. Furthermore, a feasible basis is only determined to be available if the objective function of the Phase I problem is zero as verified prior to entering Phase II.

\bigskip
\section*{Examples}
\noindent
In order to illustrate different scenarios, we consider an infeasible problem, an unbounded problem, and then finally a feasible, bounded problem:

\subsection*{Infeasible Problem}
Consider the following problem:
\[
\tag{E4.1.1}\label{e411}
\begin{array}{rrlrrlrrr}
\min & x_1&+& x_2 &\\
	& x_1 & &  & +&x_3 & = & -5; \\
	& x_1 &+&x_2& & &= & 1; \\
	& x_1 &, & x_2&\geq & 0.
\end{array}
\]
\noindent
Solving \ref{e411} results in the following output:

\bigskip
\noindent
{\tt output\_infeasible.out}:
\verbatiminput{output_infeasible.out}
\begin{center}
\line(1,0){400}
\end{center}
\bigskip

\noindent
Note that the given objective function is irrelevant in determining solution feasibility. Here Phase II is not initiated because Phase I terminated with a non-zero objective value for (\ref{phi})(in this case, it was 5).

\subsection*{Unbounded Problem}
Consider the following problem:
\[
\tag{E4.1.2}\label{e412}
\begin{array}{rrlrrlrrlrrl}
\min & -2x_1&-& x_2 &  & & & & & & \\
	& -x_1 & + &x_2 & +&x_3 & & &  = & 1; \\
	&  & - &x_2 & & & + & x_4 &  = & 2; \\
	& x_1 & , &x_2 & , &x_3 & , & x_4 &  \geq & 0. \\
\end{array}
\]

\noindent
Solving \ref{e412} results in the following output:

\bigskip
\noindent
{\tt output\_unbounded.out}:
\verbatiminput{output_unbounded.out}
\begin{center}
\line(1,0){400}
\end{center}
\bigskip

\noindent
Note that the objective function is important in determining boundedness. Here Phase II terminates because we find an extreme ray along which the objective function can decrease indefinitely. This corresponds to the basic feasible direction $\bar{z}=[1~0~1~0]'$.

\subsection*{Feasible Problem with Optimum Solution} 
Consider the following problem:
\[
\tag{E4.1.3}\label{e413}
\begin{array}{rrlrlrlr}
\min & x_1 & + & x_2 & & & & \\
	&x_1& + & 3x_2 & + &x_3&=&5;\\
	&x_1& + & x_2 & + &5x_3&=&8.\\
	&x_1& , & x_2 & , &x_3&\geq&0;\\
\end{array}
\]

\noindent
Solving \ref{e413} results in the following output:

\bigskip
\noindent
{\tt output\_feasible.out}:
\verbatiminput{output_feasible.out}
\begin{center}
\line(1,0){400}
\end{center}
\bigskip
\noindent
We trace through the steps of the algorithm below:

\begin{enumerate}
	\item We initiate the Phase I problem by considering the following augmented $A$ matrix in which the last column is $A_{n+1}=-A_{\beta}\mathbf{1}$ where $\beta$ is originally initialized as $(1,2)$.
	\[
	A_{aug}=\begin{bmatrix} 1 & 3 & 1 & -4\\
						1 & 1 & 5 & -2
						\end{bmatrix}
	\]
	The addition of $x_{n+1}$ results in $\eta=(3,4)$. Our Phase I objective is to minimize $x_{n+1}$ and hence $c=[0~0~0~1]'$.
	\item Using these data, we solve for $\bar{x}=[\frac{19}{2}~-\frac{3}{2}~0~0]'$. Because of the negative basic variable, this solution is infeasible. Therefore, we move along the direction $\bar{z}=[1~1~0~1]'$ by $\lambda=\frac{3}{2}$. Doing so results in $x_2$ being relegated from the basis and $x_{n+1}$ being promoted to the basis. Therefore,
	\[
	\beta = (1,4)
	\]
	and
	\[
	\eta=(3,2)
	\]
	Our new $\bar{x}=[11~0~0~\frac{3}{2}]$, which is feasible.
	\item Our objective value at this point is $\frac{3}{2}$. Next, we calculate $\bar{c}_{\eta}=[-2~1]'$; hence we can reduce our objective by moving along the $\eta_j=1$ direction and we conclude that $x_3$ will enter the basis. 
	\item In order to determine which variable should leave the basis, we compute $\bar{A}_{\eta_j}=[9~2]'$. Considering $\bar{x}$ from above, we determine that the largest $\lambda$ that would still result in a feasible solution would be $\frac{3}{4}$, which corresponds to removing $x_{n+1}$ from the basis. We perform the swap such that
	\[
	\beta = (1,3)
	\]
	and
	\[
	\eta = (4,2)
	\]
	\item Our objective value at this point is $0$ and hence we have solved the Phase I problem. This can be corroborated by checking $\bar{c}_{\eta}=[1~0]'\geq\mathbf{0}$, which implies that no further minimization of the objective is possible. We conclude that $\beta=(1,3)$ is a feasible basis. 
	\item In order to proceed to Phase II, we restore $A$ and $c$ to their original values. Using $\beta=(1,3)$, our objective is is $\frac{17}{4}$ and $\bar{x}=[\frac{17}{4}~0~\frac{3}{4}]'$. Next, we calculate $\bar{c}_{\eta}=-\frac{5}{2}$; hence we can reduce our objective by moving along the $\eta_j=1$ direction and we conclude that $x_2$ will enter the basis. 
	\item In order to determine which variable should leave the basis, we compute $\bar{A}_{\eta_j}=[\frac{7}{2}~-\frac{1}{2}]'$. Considering $\bar{x}$ from above, we determine that the largest $\lambda$ that would still result in a feasible solution would be $\frac{17}{14}$, which corresponds to removing $x_{1}$ from the basis. We perform the swap such that
	\[
	\beta = (2,3)
	\]
	and
	\[
	\eta = (1)
	\]
	\item Our objective value at this point is $\frac{17}{14}$, which is a reduction from the previous objective value. Furthermore, $\bar{x}=[0~\frac{17}{14}~\frac{19}{14}]'$, which remains feasible. Next, we calculate $\bar{c}_{\eta}=\frac{5}{7}$; hence we conclude that no further minimization of the objective is possible and our solution is optimal.
\end{enumerate}

\bigskip
