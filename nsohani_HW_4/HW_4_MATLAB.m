%% Worry-free Simplex Algorithm
% This function will run the worry-free Simplex algorithm, i.e. ignoring
% degeneracy and cycling.

% Get rid of all previous data

% Assume that data is stored in pivot_input

% Step 0: Start with any basic feasible partition, beta and eta:
% pivot_setup;

while(1)
     pivot_algebra;
    % c'*xbar
    % Step 1: Check if cbar_eta >=0; if yes, then stop: xbar and ybar are
    % optimal and we are done; otherwise continue
    [minval_cbar_eta, eta_j] = min(cbar_eta);

    if(minval_cbar_eta)>=0
       disp('cbar_eta >= 0; xbar and ybar are optimal.');
       break;
    end

    % Step 2: Choose a non-basic index eta_j with cbar_eta_j < 0
    % Use eta_j as defined above since it corresponds to the most minimal entry
    % in cbar_eta

    % Step 3: If Abar_eta_j<=0 then stop, P is unbounded and D is infeasible
    maxAbar_eta = max(Abar_eta,[],1);
    if maxAbar_eta(eta_j) <=0 
        disp('(P) is unbounded and (D) is infeasible.');
        break;
    end

    % Step 4: determine the index corresponding to the min lambda
    %[eta_j,~] = find(cbar_eta==max(cbar_eta(cbar_eta<0)));
    
    Abar_eta_j = Abar_eta(:,eta_j);

    ratio = xbar_beta./Abar_eta_j;

    [istar,~] = find(ratio==min(ratio(ratio>0)));

    pivot_swap(eta_j,istar);

end