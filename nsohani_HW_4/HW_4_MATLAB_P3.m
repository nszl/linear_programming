%% Worry-Free Simplex Algorithm can cycle
% Parameterization

clear;
clc

k = 5;
Q = 2*pi/k;
Y = cot(Q);

A1 = [1;0];
A2 = [0;Y];

R = [cos(Q) -sin(Q);sin(Q) cos(Q)];

A = [A1 A2];

b = [0 0]';

c(1) = 1-A(1,1)-A(2,1)/Y;
c(2) = 1-A(1,2)-A(2,2)/Y;

for j = 3:2*k
   if rem(j,2)
       A = [A R^((j-1)/2)*A1]; 
   else
       A = [A R^((j-2)/2)*A2];
   end
   c(j) = 1-A(1,j)-A(2,j)/Y;
end

c = c';
global beta eta 
beta = [4,5];

[m,n] = size(A);

eta = setdiff(1:n,beta);

pivot_algebra;
    
     
    % Step 1: Check if cbar_eta >=0; if yes, then stop: xbar and ybar are
    % optimal and we are done; otherwise continue
    [minval_cbar_eta, eta_j] = min(cbar_eta);
    eta
    cbar_eta
    if(minval_cbar_eta)>=0
       disp('cbar_eta >= 0; xbar and ybar are optimal.');
      
    end

    % Step 2: Choose a non-basic index eta_j with cbar_eta_j < 0
    % Use eta_j as defined above since it corresponds to the most minimal entry
    % in cbar_eta

    % Step 3: If Abar_eta_j<=0 then stop, P is unbounded and D is infeasible
    maxAbar_eta = max(Abar_eta,[],1);
    if maxAbar_eta(eta_j) <=0 
        disp('(P) is unbounded and (D) is infeasible.');
    end
    
    
    % Step 4: determine the index corresponding to the min lambda
    %[eta_j,~] = find(cbar_eta==max(cbar_eta(cbar_eta<0)));
    eta
    Abar_eta
 
