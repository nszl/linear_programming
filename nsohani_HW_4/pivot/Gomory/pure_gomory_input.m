% pure_gomory_input.m // Jon Lee
% data for pivoting example

A = [7 8 -1 1 3; 5 6 -1 2 1];
A= sym(A);
c = [126 141 -10 5 67]';
c=sym(c);
b = [26 19]';
b=sym(b);
beta = [1,2];
[m,n] = size(A);
eta = setdiff(1:n,beta); % lazy eta initialization