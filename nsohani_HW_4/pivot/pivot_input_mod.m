% pivot_input.m // Jon Lee
% data for pivoting example

A = [1 0 0 1 1 1 1;1 1 0 0 1 1 1;1 1 1 0 0 1 1;1 1 1 1 0 0 1;1 1 1 1 1 0 0;0 1 1 1 1 1 0;0 0 1 1 1 1 1];
A = [A -1*eye(7)];
A = sym(A);
c = [1 1 1 1 1 1 1 zeros(1,7)]';
% c = [0 3 3 2 5 7]';
% c = [16 7 20 10 4]';
c = sym(c);
% b = [0 0 0 0 0 1]';
b = [17 13 15 19 14 16 11]';
% b = [-100 2 18]';
b = sym(b);
beta = [8,9,10,11,12,13,14];
[m,n] = size(A);
eta = setdiff(1:n,beta); % lazy eta initialization 