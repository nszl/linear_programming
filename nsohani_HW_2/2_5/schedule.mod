
# schedule.mod // Adapted from Jon Lee’s flow.mod - Modified by Nauman Sohani
# 
param n:=4;

set NODES:={0..n+1};

set ARCS within NODES cross NODES;
    # TELLS AMPL TO CHECK THAT EACH ARC IS AN ORDERED PAIR OF NODES

param d {ARCS};			# duration

var t {i in NODES} >= 0;	# non-negativity constraint

minimize z:
   t[n+1]; 

subject to Duration_Precedence_Constraint {(i,j) in ARCS}:
   t[i] + d[i,j] <= t[j];
