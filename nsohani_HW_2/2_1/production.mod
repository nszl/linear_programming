
# production.mod // Jon Lee // Modified by Nauman Sohani
# 

param m integer > 0;     # NUMBER OF ROWS
param n integer > 0;     # NUMBER OF COLUMNS
set ROWS := {1..m};      # DEFINING THE SET OF ROW INDICES
set COLS := {1..n};      # DEFINING THE SET OF COLUMNS INDICES

param b {ROWS};          # RIGHT-HAND SIDE DATA
param c {COLS};          # OBJECTIVE-FUNCTION DATA
param a {ROWS,COLS};     # CONSTRAINT-MATRIX DATA

var yt {i in ROWS} >= 0;  # DEFINING THE (NONNEGATIVE) VARIABLES

minimize z:              # CHOOSE MAX/MIN AND NAME THE OBJ. FUNCTION
   sum {i in ROWS} yt[i] * b [i];  # DEFINING THE OBJECTIVE FUNCTION

subject to Constraints {j in COLS}: # DEFINING THE CONSTRAINT INDICES
   sum {i in ROWS} yt[i]*a[i,j] >= c[j]; # DEFINING THE CONSTRAINTS
