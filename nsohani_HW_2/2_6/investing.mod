
# investing.mod 
# 

param q integer > 0;		# INTEREST

param T integer > 0;		# LAST INVESTMENT PERIOD
param K integer > 0;		# NUMBER OF INVESTMENT VEHICLES

set INVESTMENTS := {1..K};	# DEFINING THE SET OF INVESTMENT INDICES
set PERIODS := {1..T};		# DEFINING THE SET OF COMPOUNDING PERIODS

param p {PERIODS};		# INFLOWS
param v {PERIODS,PERIODS,INVESTMENTS};          # INVESTMENT RETURN DATA

var x {PERIODS,INVESTMENTS} >= 0;  # DEFINING THE (NONNEGATIVE) VARIABLES

var y {PERIODS} >= 0;

maximize finalCash:              # CHOOSE MAX/MIN AND NAME THE OBJ. FUNCTION
   y[T]*(1+q/100) + sum {i in 1..T} (sum{k in 1..K} x[i,k]*v[i,T,k]);

subject to Constraint1 {t in 2..T}: # VALID FOR t>1
   p[t] + y[t-1]*(1+q/100) + sum {i in 1..t-1} (sum{k in 1..K} x[i,k]*v[i,t-1,k]) - sum{k in 1..K} x[t,k] = y[t];



subject to Constraint2: # DEFINING THE CONSTRAINT INDICES
   p[1] - sum {k in 1..K} x[1,k] = y[1];