\exercise{2.4} \emph{Mix it up}\\
\noindent
Consider the problem of creating products, $p_j$ with $i$ ingredients. Let $x_{ij}$ represent the amount of ingredient $i$ in product $j$. Then we can relate $p_j$ and its constituent ingredients using the following expression:
\[
\tag{E2.4.1}\label{e2.4.1}
p_j = \sum_{i=1}^m x_{ij}
\]
Suppose each ingredient has availability $b_i$ and we require that at least $d_j$ products be made. Furthermore, suppose each product has an upper and lower limit on the amount of each ingredient used to make that product, $u_ij$ and $l_ij$ respectively. Finally, suppose each product generates revenue of $e_j$ and each ingredient costs $c_i$. A linear optimization problem, with the objective of maximizing profits, can be formulated as:
\[
\tag{E2.4.2}\label{e2.4.2}
\begin{array}{rrrrrll}
\max & \sum_{j=1}^n \sum_{i=1}^m e_j x_{ij} & - & \sum_{j=1}^n \sum_{i=1}^m c_i x_{ij} & & &\\
	& 							    &   &				\sum_{j=1}^n x_{ij}&\leq&b_i & \forall i\\
	& 							    &   &				\sum_{i=1}^m x_{ij}&\geq&d_j &\forall j \\
	& 							    &   &	\frac{x_{ij}}{\sum_{i=1}^m x_{ij}}&\leq&u_{ij} &\forall i,j\\
	& 							    &   &	\frac{x_{ij}}{\sum_{i=1}^m x_{ij}}&\geq&l_{ij} &\forall i,j\\
\end{array}
\]

\bigskip
\noindent
In \ref{e2.4.2}, a non-zero constraint on $x_{ij}$ is implicit given a non-negative $l_ij$. Furthermore, note that a substitution was made in the objective function where the per product revenue was multiplied by the expression for $p_j$ in \ref{e2.4.1}. Since the goal is to maximize \emph{profit}, the cost of using each ingredient was deducted from the revenue.

\bigskip
\noindent
Suppose we are given the following data: 
\begin{itemize}
	\item There are three ingredients and two products;
	\item Product $1$ generates revenue of $\$10$ and product $2$ generates revenue of $\$15$;
	\item Product $1$ requires a minimum of 10\% of ingredient $1$, 10\% of ingredient $2$, and 20\% of ingredient $3$;
	\item Product $1$ allows a maximum of 60\% of ingredient $1$, 70\% of ingredient $2$, and 90\% of ingredient $3$;
	\item Product $2$ requires a minimum of 5\% of ingredient $1$, 10\% of ingredient $2$, and 25\% of ingredient $3$;
	\item Product $2$ allows a maximum of 50\% of ingredient $1$, 60\% of ingredient $2$, and 90\% of ingredient $3$;
	\item We require at least $4$ units of product $1$ and $5$ units of product $2$;
	\item There are a maximum of $4$ units of ingredient $1$ and each unit costs $\$2$;
	\item There are a maximum of $2$ units of ingredient $2$ and each unit costs $\$3$;
	\item There are a maximum of $3$ units of ingredient $3$ and each unit costs $\$4$.
\end{itemize}

\bigskip
\noindent
This problem can be solved using the following {\tt model}, {\tt data}, and {\tt run} files.

\bigskip
\noindent
{\tt mixing.mod:}
\verbatiminput{2_4/mixing.mod}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
{\tt mixing.dat:}
\verbatiminput{2_4/mixing.dat}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
{\tt mixing.run:}
\verbatiminput{2_4/mixing.run}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
Executing this program results in the output given in {\tt mixing.out:}

\bigskip
\noindent
{\tt mixing.out:}
\verbatiminput{2_4/mixing.out}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent

The program solution calls for the following:
\begin{itemize}
	\item	Make product $1$ by combining $1.5$ units of ingredient $1$, $0.75$ units of ingredient $2$, and $1.75$ units of ingredient $3$. Thus the cost of product $1$ is $\$2(1.5)+\$3(0.75)+\$4(1.75) = \$12.25$. We have a total of $1.5+0.75+1.75=4$ units of product $1$ and have thus generated $\$40$ in revenue. Our total profit from product $1$ is $\$27.25$.
	\item Make product $2$ by combining $2.5$ units of ingredient $1$, $1.25$ units of ingredient $2$, and $1.25$ units of ingredient $3$. Thus the cost of product $1$ is $\$2(2.5)+\$3(1.25)+\$4(1.25) = \$13.75$. We have a total of $2.5+1.25+1.25=5$ units of product $1$ and have thus generated $\$75$ in revenue. Our total profit from product $1$ is $\$61.25$.
	\item Our total profit is $\$89$.
	\item We have satisfied the demand requirements exactly for each product.
	\item We have used $4$ units of ingredient $1$, $2$ units of ingredient $2$ and $3$ units of ingredient $3$. Note that our entire supply was exhausted because it matched the demand requirements exactly.
	\item Product $1$ consists of $37.5\%$ of ingredient $1$, $18.75\%$ of ingredient $2$, $43.75\%$ of ingredient $3$. All of the ingredient limits have been respected.
	\item Product $2$ consists of $50\%$ of ingredient $1$, $25\%$ of ingredient $2$, $25\%$ of ingredient $3$. All of the ingredient limits have been respected.
\end{itemize}

\bigskip
\noindent
As expected, the solution maximized the amount of ingredient $2$ in product $2$ and just met the lower limit of the most expensive ingredient in product $2$. Whatever was leftover from product $2$ was then used in product $1$ because of the aforementioned supply-demand balance.

\bigskip
