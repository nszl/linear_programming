
# mixing.mod 

param m integer > 0;		# NUMBER OF INGREDIENTS
param n integer > 0;		# NUMBER OF PRODUCTS

set INGREDIENTS := {1..m};	

set PRODUCTS := {1..n};	


param e {PRODUCTS};

param c {INGREDIENTS};

param b {INGREDIENTS};

param d {PRODUCTS};


param u {INGREDIENTS, PRODUCTS};

param l {INGREDIENTS, PRODUCTS};



var x {INGREDIENTS, PRODUCTS};


maximize profit:              # CHOOSE MAX/MIN AND NAME THE OBJ. FUNCTION
	sum {j in 1..n}(sum{i in 1..m}(e[j]*x[i,j]-c[i]*x[i,j]));


subject to ingredient_constraint {i in INGREDIENTS}: 
	sum {j in 1..n} x[i,j] <= b[i];


subject to product_constraint {j in PRODUCTS}:
	sum {i in 1..m} x[i,j] >= d[j];


subject to upper_limit{i in INGREDIENTS, j in PRODUCTS}:
	x[i,j]<=(sum {k in 1..m} x[k,j])*u[i,j];

subject to lower_limit{i in INGREDIENTS, j in PRODUCTS}:
	x[i,j]>=(sum {k in 1..m} x[k,j])*l[i,j];