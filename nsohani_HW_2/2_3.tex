\exercise{2.3} \emph{Bloody {\tt AMPL}}\\
\noindent
Consider the problem of matching blood demand and supply with the objective of maximizing the versatility of the leftover blood. Blood types considered here are $\{A, B, AB, O\}$. Among these blood types, $A$ can donate to $A$ and $AB$; $B$ can donate to $B$ and $AB$; $AB$ can donate only to $AB$; and $O$ can donate to all types. Consequently, we can associate the versatility of the blood with a higher cost of moving it; therefore, minimizing this cost will preserve the most versatile blood.

\bigskip
\noindent
We can associate $A$-type blood with the index $1$, $B$-type with $2$, $AB$-type with $3$ and $O$-type with $4$. Using these indices, our objective function can be formulated as the following:

\[
\min ~ 2x_{11}+2x_{13}+2x_{22}+2x_{23}+x_{33}+4x_{41}+4x_{42}+4x_{43}+4x_{44}
\]
\noindent
\\
where $x_{ij}$ denotes the flow from the blood type of index $i$ to the blood type of index $j$; for example $x_{23}$ denotes the flow of blood from a type $B$ donor to a type $AB$ recipient.

\bigskip
\noindent
The constraints of this problem require handling the flow exceptions, which were implicit in the objective function but should be made explicit when solving for the flows. First, we need to ensure that demand at a given node is satisfied. This can be stated by the following expression: \emph{the sum of the flows into node j from all nodes i should be equal to the demand at node j}. This can be written mathematically as:
\[
\sum_{i=1}^4 x_{ij} = d_j, \hspace{1 cm} \forall j \in [1,4]
\]
\noindent
\\
Moreover, we need to ensure that supply available at a given node is not exceeded. This can be stated by the following expression: \emph{the sum of all the flows from a node i should not exceed the supply available at node i}. This can be written mathematically as: 
\[
\sum_{j=1}^4 x_{ij} \leq s_i, \hspace{1 cm} \forall i \in [1,4]
\]
\noindent
\\
Finally, to handle the flow exceptions, i.e. prevent incompatible donor-recipient blood flows, we can establish the following upper limits on flows over specific arcs:
\[
\begin{array}{rrl}
u_{12} & = & 0;\\
u_{14} & = & 0;\\
u_{21} & = & 0;\\
u_{24} & = & 0;\\
u_{31} & = & 0;\\
u_{32} & = & 0;\\
u_{34} & = & 0.\\
\end{array}
\]
Flows omitted above are unconstrained from above and are only required to be non-negative. These constraints and objectives can be summarized in the following problem statement:
\[
\tag{E2.3.1}\label{e2.3.1}
\begin{array}{rrrrrl}
\min & \sum_{i=1}^4 \sum_{j=1}^4 c_{ij}x_{ij} & &      & \\
	& \sum_{i=1}^4 x_{ij}				    &=&d_j &\forall j \in [1,4]\\
	& \sum_{j=1}^4 x_{ij}				    &\leq&s_i &\forall i \in [1,4]\\
	& 0 \leq x_{ij} \leq u_{ij}			    &      &     &\forall i,j \in [1,4]\\
\end{array}
\]
where $c_{ij}$ is summarized in the table below:

\bigskip

\begin{center}
\begin{tabular}{r|*{4}{c}}\label{cost_table}
Blood Type       & A & B & AB & O \\
$c_{ij}$		& $j=1$ & $j=2$ & $j=3$ & $j=4$\\
\hline
A, $i=1$	 & 2 & 0 & 2 & 0 \\
B, $i=2$    & 0 & 2 & 2 & 0\\
AB, $i=3$	 & 0 & 0 & 1 & 0\\
O, $i=4$	 & 4 & 4 & 4 & 4\\
\end{tabular}
\end{center}

\bigskip
\noindent
The problem given in \ref{e2.3.1} can be formulated using the following model, data, and run files in {\tt AMPL}:

\bigskip
\noindent
{\tt flow.mod:}
\verbatiminput{2_3/flow.mod}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
{\tt flow.dat:}
\verbatiminput{2_3/flow.dat}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
{\tt flow.run:}
\verbatiminput{2_3/flow.run}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
Executing this program results in the output given in {\tt flow.out:}

\bigskip
\noindent
{\tt flow.out:}
\verbatiminput{2_3/flow.out}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
This example problem called for the following: we require 10 units of type $A$ blood, 50 units of type $B$ blood, 80 units of type $AB$ blood and 20 units of type $O$ blood. The supply available to satisfy this demand is: 40 units of type $A$ blood, 40 units of type $B$ blood, 10 units of type $AB$ blood and 100 units of type $O$ blood.

\bigskip
\noindent
Before even executing the solver, we notice that the demand for type $O$ blood can only be satisfied by type $O$ blood; consequently we expect any feasible solution to call for $x_{44} = 20$. Moreover, our supply exceeds our demand; therefore, we desire that the remaining blood will be the most versatile combination, with ideally as much type $O$ blood as possible. Note that the supply of $AB$ exceeds the demand of $AB$; therefore, we expect $x_{33}=10$ as this will minimize our objective.

\bigskip
\noindent
The proposed solution calls for the following:
\begin{itemize}
  \item No blood from $A$ to $A$;
  \item 40 units of blood from $A$ to $AB$;
  \item 40 units of blood from $B$ to $B$;
  \item No blood from $B$ to $AB$;
  \item 10 units of blood from $AB$ to $AB$;
  \item 10 units of blood from $O$ to $A$;
  \item 10 units of blood from $O$ to $B$;
  \item 30 units of blood from $O$ to $AB$;
  \item 20 units of blood from $O$ to $O$.
\end{itemize}

\bigskip
\noindent
Note that the blood compatibility requirements are respected. Furthermore, the excess blood is all of type $O$, which intuitively satisfies our expectations. Finally the cost of the blood transfer can be computed as $2*(40)+2*(40)+1*(10)+4*(10+10+30+20)=450$, which is the value of the objective function.

\bigskip