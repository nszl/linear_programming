\exercise{2.5} \emph{Task Scheduling}\\
\noindent
Consider the problem of task scheduling with the objective of minimizing the start time of the end task. If task $i$ represents the $i^{th}$ task with start time $t_i$, we need to find the optimal start times of each task given that each task has duration $d_i$ and precedence $\Psi_i$, which represents the set of tasks that must be completed before initiating task $i$.

\bigskip
\noindent
While the objective is clear, i.e. $\min~ t_{n+1}$, there is a question of how to treat the task durations and precedences. Firstly, we note that tasks can be carried out in parallel. The precedences essentially serve to order the tasks in this parallel structure and impose a condition on when the $i^{th}$ task can begin. This idea can be captured by the following expression: \emph{the $i^{th}$ task cannot begin until all the precedences have been completed.} This statement can be written mathematically as:
\[
\tag{E2.5.1}\label{e2.5.1}
t_i \geq t_j + d_j \hspace{1 cm} \forall j \in \Psi_i
\]
where the expression $t_j+d_j$ captures the idea of completing a precedence. Moreover, we note that $t_0=0$ and $d_0=0$. Here, $t_0$ is not a decision variable. In order to formulate the problem clearly, we should also place some restriction on the set $\Psi_i$. If we require that 
\[
j<i \hspace{1 cm} \forall j \in \Psi_i ~and~ \forall i \in [1,n+1]
\]
then the data is well-conditioned to form a linear programming problem. This also places a restriction on the first task start time $t_1=0$ because the duration of the start task is $0$ and there can only be the $0$ precedence for $i=1$.
\bigskip
\noindent
Using \ref{e2.5.1}, we can express the task scheduling problem as the following linear optimization problem:
\[
\tag{E2.5.2}\label{e2.5.2}
\begin{array}{rrrrrrl}
\min & t_{n+1}	&	 &      & 	&	& \\
	& t_j	& +	& d_j	& \leq & t_i & \forall i \in [1,n+1],~\forall j \in \Psi_i\\
	& t_i		&     & 	& \geq & 0   & \forall i \in [1,n+1]\\					
\end{array}
\]
where it is assumed that the aforementioned restriction on $\Psi_i$ is in place.

\bigskip
\noindent
To solve \ref{e2.5.2}, we can conceptualize the durations as arcs between nodes $i$ and $j$. For example, suppose we have the following data:
\[
\begin{array}{ccc}
i	&	d_i		&	\Psi_i\\	\hline
1	&	3		&	\{0\}	\\	
2	&	2		&	\{1\}	\\	
3	&	5		&	\{1\} \\	
4	&	7		&	\{2,3\} \\	
5	&	0		&	\{4\}	\\	
\end{array}
\]

\bigskip
\noindent
We can describe the duration and precedences using the following nodes and arcs:
\[
\begin{array}{cc}
Arc ~ (i,j)	&	e_{ij}		\\	\hline
(0,1)		&	0		\\
(1,2)		&	3		\\
(1,3)		&	3		\\
(2,4)		&	2		\\
(3,4)		&	5		\\
(4,5)		&	7		\\
\end{array}
\]

\bigskip
\noindent
This problem can be solved using the following {\tt model}, {\tt data}, and {\tt run} files.

\bigskip
\noindent
{\tt schedule.mod:}
\verbatiminput{2_5/schedule.mod}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
{\tt schedule.dat:}
\verbatiminput{2_5/schedule.dat}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
{\tt schedule.run:}
\verbatiminput{2_5/schedule.run}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
Executing this program results in the output given in {\tt schedule.out:}

\bigskip
\noindent
{\tt schedule.out:}
\verbatiminput{2_5/schedule.out}
\begin{center}
\line(1,0){400}
\end{center}

\bigskip
\noindent
The program solution calls for initiating task 1 at $t_1=0$, and then performing both tasks 2 and 3 in parallel since these both depend upon the completion of task 1 at times $t_2=t_3=3$. Since task 4 depends upon both tasks 2 and 3, we must wait until both are completed before initiating task 4. Consequently, $t_4=t_3+5=8$. Since task 5, cannot be initiated until task 4 is completed, $t_5 = t_{n+1}=t_4+7=15$.

\bigskip